# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 17:00:20 2021

@author: Dunaev


Модуль запуска алгоритмов

Взаимодействие с системой испытаний:
    Параметры запуска:
        "START" : перед началом расчета. 
        "CALC" : Шаг расчета
        "DONE" : Рассчитать итоговые данные

"""

import algorithm

# режим автономной отладки модулей алгоритма
ALG_DEBUG           = 0


def runAlgorithm(algName, param, debug=0):
    print("RUN ALG[%s, param: %s, [debug:%d]]" % (algName, param, debug))
    if (algName == "RFGen"):
        result = algorithm.algorithmRFGen(param, debug)
    return result


if ALG_DEBUG == 1:
    print("\n !!! Режим отладки алгоритма !!! \n")
    runAlgorithm("RFGen", "START", debug=1)
    runAlgorithm("RFGen", "CALC", debug=1)
    runAlgorithm("RFGen", "DONE", debug=1)
