// Это не js! Это Ti Script
// .js - для адекватной подстветки синтаксиса
  include "decorators.tis";

// Событие при нажатии пользователем на кнопки-индакаторы LedSw ["div.cell"; "div.cell1.." ]
 	@click @on "div.cell"; "div.LedSw" : evt
	{
    var code = 0;
	var cmd = evt.target.attributes#cmd; //Обозначение команды
	var run = evt.target.attributes#run; //Обозначение проверки, которую нужно запустить

	// проверка на тип (актуально если на обработчик вешать несколько типов)
	// if (e.target.attributes#class == "LedSw") 
	if (evt.target.value) code = 1;
	
	if (cmd == "undefined")	{
	  // В кнопке нет поля cmd
		if (run == "undefined")	{
			// В кнопке нет полей run и cmd- в данном обработчике ничего не делаем.
			// [ резерв ]
			// --------------------------------------------------------------------
		} else {
			// запуск теста
			LOG(String.printf("RUN: %s, %v", run, evt.target.text));
			RunTest(run, evt.target.text)
		}
	}
	else {
		// произвольная команда
		if (evt.target.attributes#param != "undefined") {
			cmd = String.printf("%s %v", cmd, evt.target.attributes#param)
			}
		LOG(String.printf("CMD: [%s] {%v}", cmd, evt.target.text));
		SendCommand("CMD", cmd)
		}
	} 
	
	
