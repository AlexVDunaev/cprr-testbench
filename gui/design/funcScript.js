// Это не js! Это Ti Script
// .js - для адекватной подстветки синтаксиса
// Поведение формы
	var isServerConnected = 0; // Есть ли связь с сервером испытаний
	var fieldNum = 0;
	var widthTable = 4; // кол-во колонок в таблице параметров
	function addTable() {
		var s = "<table .ATM><tr>";
		for (var i = 0; i < widthTable; i++) 
			s = s + String.printf("<th> <code id=\"W%d\">%s</code></th> ", fieldNum + i, "---");
		s = s + "</tr><tr>"
		for (var i = 0; i < widthTable; i++) 
			s = s + String.printf("<td> <code id=\"V%d\">%s</code></td> ", fieldNum + i, "0");
		s = s + "</tr></table>"
		// добавляем табличку для отображения параметров
		fieldNum = fieldNum + widthTable;
		self#TABLE.html = self#TABLE.html + s;
	}
	
	var runningTestName = "";

	function ExitTest() {
		self#statusTest.style#visibility = "none";    // ["visible" | "none"]
		self#selectTest.style#visibility = "visible"; 
		runningTestName = "";
	}

	function RunTest(value, label) {
		SendCommand("RUN", value)
		if (value == "BREAK") {
			// BREAK - служебное имя для прерывания проверки 
			LOG(String.printf("BREAK: [%s]", runningTestName));
			ExitTest();
		} else {
			runningTestName = label;
			self#selectTest.style#visibility = "none"; 
			self#statusProgress.style#visibility = "none";
			self#statusTest.style#visibility = "visible";
			self#currentTest.text = label;
			}
	}
	
	function SendCommand(cmd, value) {
		// зарезервированная команда 
		if (cmd == "CMD")
			if (value == "EXIT")
			  view.close();
	    // --------------------------
		if (isServerConnected == 1) {
			view.root.serviceCmd(cmd, value)
			}
	}

	function ViewVarValue(varId, varName, varValue) {
		var currField;
		currField = self.select(String.printf("#W%d", varId));
		if (currField != "undefined")
			currField.text = varName;
		currField = self.select(String.printf("#V%d", varId));
		if (currField != "undefined")
			currField.text = varValue;
	}

	function SetVarValue(varId, varName, varValue) {
		isServerConnected = 1;
		if (varId < 0) {
			if (varName == "CONNECT") {
				self#STATUS.text = " (" + varValue + ")";
			}
			if (varName == "CLEAR") {
				fieldNum = 0;
				self#TABLE.html = "";
			}
			if (varName == "TESTDONE") {
				ExitTest();
			}
			if (varName == "PROGRESS") {
				self#ProgressNum.text = varValue;
				self#statusProgress.style#visibility = "visible";
			}
			if (varName == "LOG") {
				var r = varValue.match(/(.+)\{.*color="([\w\d#]+)".*\}.*/);
				if (r) {
					LOG(r[1], r[2]);
					} else {
					LOG(varValue);
					}
				}
			return 0;
		}
		if (varId >= fieldNum) {
			addTable();
		}
		ViewVarValue(varId, varName, varValue);
		return 0;
	}

	function LOG(value, color = "Gray") {
		var list = $(ol.log);
		list.$append( <li style="color:{color}">{value}</li>);  	  
		$(ol.log).last.scrollToView();
	}

    function ShowDialogWin(A) {
		// A : url[: any text]
		var dialogParam = A.match(/(.+)\:(.*)/);
		if (dialogParam) {
			A = dialogParam[1];
			dialogParam = dialogParam[2]; //String.printf("dialogParam: [%s]", dialogParam[2]);
		} else {
			// dialogParam = ""
		}
        view.window {
                type: View.FRAME_WINDOW,
                 url: self.url(A),
               state: View.WINDOW_SHOWN,
               width: 800,
              height: 700,
           alignment: 5, // center of the screen  
		  parameters: { // view.parameters inside new window
		    dialogParam : dialogParam,
			LOG : LOG,
			RunTest : RunTest,
			SendCommand : SendCommand
          }

        };
	}


	addTable();
	SetVarValue(-1, "LOG", 'GUI Started!{color="#808080"}');
//	ShowDialogWin("design/rfgenDone.htm")
