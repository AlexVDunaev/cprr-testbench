
$(#OK).on(    "click", function(e){view.parameters.SendCommand("DIALOG", "OK");   view.close()});
$(#CANCEL).on("click", function(e){view.parameters.SendCommand("DIALOG", "BREAK");	view.close()});

	// адаптивный размер под изображение #png (и ограничение на его уменьшение)
	var baseImage = self.loadImage(self.url(self#png.attributes["src"]));
	view.windowMinSize = (baseImage.width + 80, baseImage.height + 260);
	
	if (view.parameters.dialogParam) {
		self#ErrorText.text = view.parameters.dialogParam;
	}