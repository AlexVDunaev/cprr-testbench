# -*- coding: utf-8 -*-
"""
Created on 2021.04.xx

@author: Dunaev A.V.
"""

import sciter           # GUI
from time import sleep
from threading import Thread
import socket
import re
from algRunner import runAlgorithm

#  Приложение-клиент для испытательного стенда CPRR-24 (мониторинг стенда).
#  ---------------------------------------------
#  Формат взаимодействия с сервером (транспорт: TCP:5455):
#  1. Сообщения текстовые. Разделяются символом \n (код 10)
#  2. Сообщения имеют вид: "[ANYCMD] ANYTEXT\n" (ANYCMD = [\w], ANYTEXT = [.*])
#  3. Если GUI получает сообщение без поля команды ([]) --> [LOG] - это сообщение вывода в лог.
#     "TEXT\n" - эквивалентно "[LOG] TEXT\n"
#  4. Запись в поле параметров имеет формат '[TM] "ANY VAR NAME" ANY STR VALUE\n'
#  5. Особые команды получаемые от сервера испытаний:
#    '[GUI] "CLEAR" 1\n' - Очистка таблицы параметров сост. системы (GUI в ИСХ. сост.)
#    '[GUI] "PROGRESS" TEXT\n' - Вывод прогресса выполнения проверки
#    '[GUI] "TESTDONE" 0/1\n'  - Уведомление, что проверка завершена (0/1 - статус)
#
#  6. Команды которые уходят на сервер
#    '[CMD] ID\n' - общий формат
#    Особые команды:
#    '[CMD] NAME\n'  - запуск проверки
#    '[CMD] BREAK\n' - прервать текущую проверку
#    '[CMD] EXIT\n'  - Команда закрыть сервер и закрытья самим
#
#

#  Шаблон для обработки сообщения от сервера
messagePattern = re.compile('^\[(\w+)\]\s*(.*)')

#  Шаблон для обработки поля значения сообщения типа [TM]/[GUI]
TmPattern = re.compile('"(.+)"\s*(.*)')

#  Флаг
monitoring_on = 0

#  Словарь, для хранения имен параметров GUI
TM = {}

#  Сокет взаимодействия с сервером испытаний
monSock = None
#  Порт сервера испытаний
MONITORING_PORT = 5455  # порт, на который приходят данные от программы
#  параметры соединения с сервером (локально)
SERVER = ("localhost", MONITORING_PORT)  # пара ip/port для чтения данных


#  Упаковка и отправка сообщений серверу.
#  Если GUI посылает команду EXIT, тоже завершим работу!

def sendServiceCMD(strcmd, strValue):
    global monitoring_on
    pkt = "[" + strcmd + "] " + strValue + "\n"
    pkt = bytes(pkt.encode('utf-8'))
    if strcmd == "CMD" and strValue == "EXIT":
        monitoring_on = 0
    if (monSock):
        monSock.send(pkt)
    pass


class RootEventHandler(sciter.EventHandler):
    def __init__(self, el, frame):
        super().__init__(element=el)
        self.parent = frame
        pass

    def on_event(self, source, target, code, phase, reason):
        #  возможность обработки событий от GUI (пока не нужно)
        # he = sciter.Element(source)
        pass

#  регистрирую функцию method_call (как serviceCmd) в sciter TIScript
    @sciter.script("serviceCmd")
    def method_call(self, *args):
        sendServiceCMD(args[0], args[1])
        return sciter.Value.null()
    pass

# ------------------------------------------------------------------------------

#  Класс-обертка для создания потоков
class MyThread(Thread):
    def __init__(self, name, func):
        Thread.__init__(self)
        self.name = name
        self.func = func

    def run(self):
        print('Thread start: %s' % self.name)
        self.func()
# ------------------------------------------------------------------------------

#  Функция создания окна и обработки сообщений (выход по view.close())
def sciterRun(winSize):
    global frame
    sciter.runtime_features(allow_sysinfo=True)
    frame = sciter.Window(ismain=True, uni_theme=True, size=winSize)
    frame.load_file("monitoring.htm")
    ev2 = RootEventHandler(frame.get_root(), frame)
    monitoring_value(-1, "CONNECT", "CONNECTING...")
    #  Выход их run_app() только после закрытия окна GUI
    frame.run_app()
    pass

#    sciter.runtime_features(file_io=True, allow_sysinfo=True)
 #   frame = sciter.Window(ismain=True, uni_theme=True)
  #  frame.load_file("examples/minimal.htm")
   # frame.run_app()


# ------------------------------------------------------------------------------

#  передать параметр для отображения скрипту GUI
def monitoring_value(num, title, value):
    global frame
    frame.call_function('SetVarValue', num, title, value)


#  функция обработки сообщения вида: "[ANYCMD] ANYTEXT\n"
def monitoring_data(data):
    try:
        result = messagePattern.findall(data)
        if not result:
            monitoring_value(-1, "LOG", data)
        else:
            (groupCmd, value) = (result[0][0], result[0][1])
            if groupCmd == "LOG":
                monitoring_value(-1, "LOG", value)
            if groupCmd == "TM":
                kv = TmPattern.findall(value)
                if kv:
                    (k, v) = (kv[0][0], kv[0][1])
                    if TM.get(k) is None:
                        TM[k] = len(TM)
                    monitoring_value(TM[k], k, v)
            #  Специальные команды управления GUI (по формату как и [TM])
            if groupCmd == "GUI":
                kv = TmPattern.findall(value)
                if kv:
                    (k, v) = (kv[0][0], kv[0][1])
                    monitoring_value(-1, k, v)
                    if k == "CLEAR":
                        TM.clear()
                    if k == "DIALOG": #url:params
                        frame.call_function('ShowDialogWin', v)
            #  Специальные команды запуска алгоритмов
            if groupCmd == "ALG":
                kv = TmPattern.findall(value)
                if kv:
                    (k, v) = (kv[0][0], kv[0][1])
                    result = runAlgorithm(k, v)
                    sendServiceCMD("ALG", ("%s" % result))
    except Exception as e:
        print(e)


#  поток обслуживания сокета
def monitoring_listener():
    global monitoring_on
    global monSock
    global SERVER
    monitoring_on = 1

    while monitoring_on == 1:
        #  цикл установки соединения с сервером
        while monitoring_on == 1:
            try:
                print('monitoring>connect...')
                monSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                monSock.settimeout(1.5)
                monSock.connect(SERVER)
            except socket.timeout:
                print('monitoring>connect timeout...')
                monSock.close()
                continue
            except socket.error as error:
                print(error)
                monitoring_on = 0
                break
            print('monitoring>connect ok')
            monitoring_value(-1, "CONNECT", "CONNECT - OK")
            break

        #  буфер для хранения части фрагментированного сообщения            
        strBuffer = ""
        #  цикл приема сообщений
        while monitoring_on == 1:
            monSock.settimeout(1.5)
            monSock.setblocking(0)
            try:
                data = monSock.recv(1024)
                if not data:
                    #  возникает в случае ошибки связи
                    print('not data...-->reconnect')
                    break
                strBuffer = strBuffer + str(data, encoding = 'utf-8')
                strLines = re.split('\n', strBuffer)
                print(strLines)
                # в буфере оставим хвост сообщений, я если оно фрагментировано
                strBuffer = strLines[-1]
                #  обработка сообщений
                for cmd in strLines:
                    if cmd > "":
                        monitoring_data(cmd)
                #  Если формат не тот, не даю расти строке больше 1Кб
                if len(strBuffer) > 1024:
                    strBuffer = ""
                    print('len(strBuffer) > 1024')
            except socket.timeout:
                print('monitoring>timeout...')
                if monitoring_on == 0:
                    break
                continue
            except socket.error as error:
                #  Операция на незаблокированном сокете не может быть завершена
                if (error.errno == 10035):
                    sleep(0.01)
                    continue
                print(error)
                break
        #  сюда вышли потому что возникла ошибка на сокете
        monitoring_value(-1, "CONNECT", "DISCONNECT")
        monSock.close()
        monSock = None
    #  вышли из всех циклов, поток завершается
    pass


def monitoring():
    #  даю время запуститься GUI
    sleep(1)
    print('monitoring thread starting...')
    monitoring_listener()
    print('monitoring thread stop')


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

monitoring_thread = MyThread('monitoring', monitoring)
monitoring_thread.start()

print('GUI start ...')
#  размер окна: 1000 x 800
sciterRun([1000, 800])
monitoring_on = 0  # сигнал окончания работы
print('monitoring close wait...')
monitoring_thread.join()

print('Exit...')
