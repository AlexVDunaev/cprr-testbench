# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 17:00:20 2021

@author: Dunaev


Алгоритм построения АЧХ:
Имеем набор кадров, каждый кадр снят на фиксированной частоте генератора.
Радар работает в спец. режиме, в котором отключены передатчики и вместо ЛЧМ,
выдается фиксированная частота (около 24.0545ГГц). АЦП работает в обычном режиме.
Генератор устанавливается на частоту ~ 24.0545ГГц и с каждым кадром уменьшая ее,
При этом на входе АЦП поступает сигнал биений как разность этих частот. 
Т.о. проходя генератором диапазон частот, получаем возможность снять АЧХ.

Взаимодействие с системой испытаний:
    Параметры запуска:
        "START" : перед началом расчета. При этом в систему испытаний (СИ)
        нужно передать путь к рабочей директории алгоритма. 
        В данную директорию СИ будет размещать файл данных (rfFrameBuff.bin),
        который открыт в режиме memory map (mmap) и содержит 1 кадр данных радара.
        Так же из этой директории СИ будет забирать файлы графиков. 
        Возврат: Путь к рабочей директории
        
        "CALC" : Очередной кадр снят радаром и размещен в файл данных (mmap).
        На данном этапе нужно рассчитать FFT по каждому чирпу/каналу и сохранить
        данные расчета в буфере (для финального расчета). 
        При этом, число вызовов "CALC" равно числу кадров данных и равно числу изменений частоты и 
        заранее не задано. 
        Возврат: ["OK" | "ERROR"] - статус (пока игнорируется)

        "DONE" : Все кадры обработаны, нужно построить АЧХ, используя накопленные данные.
        Используя ранее накопленные данные находим максимум для каждого кадра, при этом,
        первые точки FFT обрезаются, т.к. амплитуда сигнала возле 0 частот превышает пики.
        По полученным максимумам (их число равно числу кадров) строится график АЧХ.
        Алгоритм должен создать след. файлы графиков:
        rfgenResult.png - обобщенный график все каналы, все чирпы. Если что-то не так, на нем все видно!
        rfgenResultAF1.png - АЧХ каналов 1..16
        rfgenResultAF2.png - АЧХ каналов 17..32
        rfgenResultEx_0.png - Результат FFT (наложение по кадрам)
        ...
        rfgenResultEx_15.png
        Возврат: ["OK" | "ERROR"] - статус (пока игнорируется)


"""

import os
import numpy as np
import matplotlib.pyplot as plt
import mmap
import math
import array
import struct
from shutil import copyfile
import shelve


FRAME_HEADER_SIZE   = 52
# Параметры формата данных (как в алгоритме матлаба)
CHIRP_SAMPLE_OFFSET = 14 
CHIRP_SAMPLE_NUM    = 670
RADAR_RX_CHANNEL    = 16
RADAR_TX_CHANNEL    = 2
# первые чирпы содержат перех.процесс. они отбрасываются
CHIRP_DROP_NUM      = 4
FFT1_SIZE           = 1024
FFT1_SCALE          = 1 # (1/FFT1_SIZE)
# кол-во точек вначале спектра, которые обнуляю, что бы не мешали обнаружить пик на нужной частоте
ZERO_HEAD_PADDING   = 10

M_PI                = 3.14159265358979

# параметр DPI для figure. Значение больше: элементы крупнее.
dpi = 109 

# размер графиков в пикселях (делаю все графики одинаковые по размеру)
PNG_SIZE_X          = 1200
PNG_SIZE_Y          = 650

# путь к GUI (Сводный график используется для GUI)
DIRECTORY_GUI_PNG = "./design"
# относительный путь к рабочей директории
DIRECTORY_WORK = "./algorithm/workdir"
# относительный путь к директории с отладочн. массивами
DIRECTORY_DEBUG = "./algorithm/dbg"
# формат сетки графика
Y_SCALE = "linear"  # ["log" | "linear"]


def algorithmPrint(text, error=0):
    print("algorithm[АЧХ]>", text)
    pass


def mmap_io(filename):
    with open(filename, mode="rb") as file_obj:
        with mmap.mmap(file_obj.fileno(), length=0, access=mmap.ACCESS_READ) as mmap_obj:
            buff = mmap_obj.read()
            return buff


# Для автономной отладки алгоритма (без радара/генератора)
# Используя радар и генератор, сохраняю ("save") данные измерений,
# Не имея радара эти измерения можно использовать для отладки ("load")
# action: ["save" | "load" | "anytext" - для возврата переменной без изменения
def dbgSerialize(filename, action, var):
    result = var
    if action == "save":
        # Сохраню накопл. данные для послед. анализа
        s = shelve.open(filename)
        # Кол-во точек измерения
        s['N'] = len(var)
        for i, r in enumerate(var):
            s[str(i)] = r
        s.close()
    if action == "load":
        s = shelve.open(filename)
        n = s['N']
        result = [[] for _ in range(n)]
        for i in range(n):
            result[i] = s[str(i)]
        s.close()
    return result


# формирование окна Хемминга (алгоритм как в матлабе)
def getRangeWin(N):
    win = []
    meanValue = 0
    for i in range(N):
        value = 0.54 - 0.46 * math.cos(2.0 * M_PI * i / (N - 1))
        win.append(value)
        meanValue += value
    meanValue = meanValue / N
    for i in range(N):
        win[i] = win[i] / meanValue
    return win


# сохранение изображения графика в файл
def exportFigure(plt, filename):
    try:
        plt.savefig(filename)
        algorithmPrint(filename + "...OK")
    except PermissionError:
        algorithmPrint('save fig Error...', error=1)
    pass


# формируем метки на оси X
def getXticks(N, step=1, first=0):
    return [x*step + first for x in range(N)]


# Общее оформление графиков
def getGraph(title):
    fig = plt.figure(dpi=dpi, figsize=(PNG_SIZE_X / dpi, PNG_SIZE_Y / dpi))
    ax = fig.add_subplot(111)  # 111 - это сетка. 111 означает сетка 1x1
    ax.set_yscale(Y_SCALE)
    ax.set_title((title))
    ax.grid()
    return fig, ax


# Функция алгоритма.
def algorithmRFGen(param, debug=0):
    # окно для FFT
    global rangeWin
    # Максимальные значения по всем кадрам (новый кадр - новая частота)
    global resultMaxMeanFFTAllFrames
    # Средние значения FFT для каждого кадра отдельно
    global resultMeanFFTFrame
    global RADAR_RX_CHANNEL
    global workDirectory

    if (param == "START"):
        resultMaxMeanFFTAllFrames = []
        resultMeanFFTFrame = []
        rangeWin = getRangeWin(CHIRP_SAMPLE_NUM)
        rangeWin.extend(np.zeros(FFT1_SIZE - CHIRP_SAMPLE_NUM))

        copyfile(DIRECTORY_GUI_PNG + "/rfgenResultErr.png",
                 DIRECTORY_GUI_PNG + "/rfgenResult.png")

        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + DIRECTORY_WORK
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        return workDirectory

    if (param == "DONE"):
        algorithmPrint("DONE")
        if (len(resultMaxMeanFFTAllFrames) > 0):
            # построение сводки (только для GUI)
            resultMaxMeanFFTAllFrames = np.max(resultMaxMeanFFTAllFrames, axis=0)
            resultMaxMeanFFTAllFrames = 20*np.log10(resultMaxMeanFFTAllFrames)
            x = getXticks(len(resultMaxMeanFFTAllFrames))
            fig, ax = getGraph("Макс. по всем FFT для всех приемных каналов")
            ax.plot(x, resultMaxMeanFFTAllFrames)  # график спектра
            ax.legend(loc='upper right')
            exportFigure(fig, DIRECTORY_GUI_PNG + "/rfgenResult.png")
            copyfile(DIRECTORY_GUI_PNG + "/rfgenResult.png",
                     workDirectory + "/rfgenResult.png")

            # График максимальных значений.
            # Получаем параметры осей (будем применять их к остальным графикам)
            x1, x2, y1, y2 = ax.axis()

            if debug == 1:
                # загружаю данные из файла
                resultMeanFFTFrame = dbgSerialize(DIRECTORY_DEBUG + "/rfResultEx.bin", "load", resultMeanFFTFrame)

            # переведем значения в log - формат
            resultMeanFFTFrame = 20 * np.log10(resultMeanFFTFrame)

            # Возьмем максимальные значения (все кадры в один)
            rfResultExMax = np.max(resultMeanFFTFrame, axis=0)

            # построение детальных графиков (по каждому каналу отдельно)
            for channel in range(RADAR_RX_CHANNEL):
                fig, ax = getGraph('Макс. по всем FFT приемного тракта канала №%d' % (channel+1))
                ax.plot(x, rfResultExMax[channel], label=('RX %d, TX = 1' % (channel+1)))
                ax.plot(x, rfResultExMax[channel + RADAR_RX_CHANNEL], label=('RX %d, TX = 2' % (channel+1)))
                # корректно активировать легенду можно только после ax.plot()
                ax.legend(loc='upper right')
                ax.axis((x1, x2, y1, y2))
                exportFigure(fig, workDirectory + ("/rfgenResultEx_%d.png" % channel))

            # кол-во кадров (шагов частоты)
            FREQ_POINT = len(resultMeanFFTFrame)

            x = getXticks(FREQ_POINT)

            # поиск максимума для данной частоты (кадра)
            maxAmp = []
            for a in resultMeanFFTFrame:
                # Нужно "занизить" точки возле 0, что бы найти пик
                for ch in range(RADAR_RX_CHANNEL * RADAR_TX_CHANNEL):
                    for i in range(ZERO_HEAD_PADDING):
                        a[ch][i] = -120
                # поиск максимума
                m = np.max(a, axis=1)
                maxAmp.append(m)

            # список максимальных значений FFT по каждому кадру
            maxAmp = np.transpose(maxAmp)

            # Построение результирующих графиков (АЧХ)
            # На практике обнаружилось отличие первых 16 каналов от вторых,
            # поэтому сохраняю их раздельно
            for tx in [1, 2]:
                fig, ax = getGraph("АЧХ всех приемных каналов 1..16 TX%d(max)" % tx)
                for i in range(RADAR_RX_CHANNEL):
                    ax.plot(x, maxAmp[i + RADAR_RX_CHANNEL * (tx - 1)], label=('RX %d' % (i + 1)))
                ax.legend(loc='upper right')
                ax.axis((1, FREQ_POINT, y1, y2))
                exportFigure(fig, workDirectory + "/rfgenResultAF%d.png" % tx)
            plt.close('all')
        else:
            algorithmPrint("no data!", error=1)
        return "DONE"

    if (param == "CALC"):
        if debug == 1:
            # загружаю данные из файла
            fileName = ("%s/rfFrameBuff.bin" % DIRECTORY_DEBUG)
        else:
            fileName = ("%s/rfFrameBuff.bin" % workDirectory)

        # Получаем данные от системы испытаний.
        # (полный кадр стрима, включая заголовок)
        data = mmap_io(fileName)

        # Если данных меньше чем заголовок - ошибка!
        if (len(data) < FRAME_HEADER_SIZE):
            # Ошибка формата данных.
            algorithmPrint("MMAP ERROR!!! Data size:%d" % (len(data)), error=1)
            return -1

        # Парсим заголовок кадра. В нем содержится информация
        # о кол-ве точек на чирп (возможно 720 и 378)
        STX, LEN, T, TIME, GN, T1, T2, T3, T4, T5, T6, N_CHIRP, N_CH, N = struct.unpack_from( "<iiiqqhhhhhhIII", data, 0)

        algorithmPrint("FORMAT: [%d x %d x %d]" % (N_CHIRP, N_CH, N))

        RADAR_RX_CHANNEL = N_CH
        
        # Кол-во рабочих чирпов (без MIMO), которые идут в алгоритм
        CHIRP_NUM = N_CHIRP - CHIRP_DROP_NUM

        # Исходя из данных заголовка,
        # отбрасываем мусорные чирпы (N : кол-во точек АЦП в чирпе)
        drop_data_size = N * CHIRP_DROP_NUM * N_CH * RADAR_TX_CHANNEL
        adc_raw_array = array.array('h', data[FRAME_HEADER_SIZE + drop_data_size:])

        # проверка на корректность размера данных
        if (len(adc_raw_array) != N * CHIRP_NUM * N_CH):
            algorithmPrint("DATA FORMAT ERROR!!!", error=1)
            return 0

        # массив чирпов без привязки к каналам и доплеру (1024 чирпа)
        chirps = []
        # формирую список из 32 пустых списков (один список на канал (MIMO))
        fft = [[] for _ in range(RADAR_RX_CHANNEL * RADAR_TX_CHANNEL)]
        # дополнение чирпа нулями, как в матлабе
        zeros = np.zeros(FFT1_SIZE - CHIRP_SAMPLE_NUM).astype(int)

        # разбивка массива int16 на список чирпов (берем только полезную часть)
        # [xxxxxxADCADCADCADCADCADC...ADCADCxxxxx] - Массив выборок АЦП 
        #        ^ <- CHIRP_SAMPLE_NUM  -> ^ - кол-во полезных выборок АЦП
        #        ^CHIRP_SAMPLE_OFFSET - Смещение полезных выборок АЦП
        for n in range(len(adc_raw_array) // N):
            chirp = adc_raw_array[n*N + CHIRP_SAMPLE_OFFSET:n*N + CHIRP_SAMPLE_NUM + CHIRP_SAMPLE_OFFSET]
            # дополнение нулями
            chirp.extend(zeros)
            # наложение окна
            chirps.append(np.multiply(chirp, rangeWin))

        # расчет FFT1 по чирпам. Результат группируется по каждому каналу 1..32
        for i, chirp in enumerate(chirps):
            fft[i % 32].append(np.abs(FFT1_SCALE * np.fft.rfft(chirp)))

        # беру среднее по всем чирпам для каждого канала (вдоль Доплера)
        fftmean = np.mean(fft, axis=1)

        # для обобщенной статистики, беру максимальное значение из 32-х каналов
        fftmeanCommon = np.max(fftmean, axis=0)

        # Добавляем данные в список кадров
        resultMaxMeanFFTAllFrames.append(fftmeanCommon)
        resultMeanFFTFrame.append(fftmean)

    return "OK"

# режим автономной отладки модуля через algRunner.py 
# ALG_DEBUG           = 1
#
#def runAlgorithm(algName, param, debug=0):
#    print("RUN ALG[%s, param: %s, [debug:%d]]" % (algName, param, debug))
#    if (algName == "RFGen"):
#        result = algorithmRFGen(param, debug)
#    return result
#
#
#if ALG_DEBUG == 1:
#    print("\n !!! Режим отладки алгоритма !!! \n")
#    runAlgorithm("RFGen", "START", debug=1)
#    runAlgorithm("RFGen", "CALC", debug=1)
#    runAlgorithm("RFGen", "DONE", debug=1)
