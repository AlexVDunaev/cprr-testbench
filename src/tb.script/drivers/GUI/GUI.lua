--[[

TCP (Server) : port 5455
Формат взаимодействия:
1. Сообщения текстовые. Разделяются символом \n (код 10)
2. Сообщения имеют вид: "[ANYCMD] ANYTEXT\n" (ANYCMD = [\w], ANYTEXT = [.*])
3. Если GUI получает сообщение без поля команды ([]) --> [LOG] - это сообщения вывода в лог.
4. Запись в поле параметров имеет формат '[TM] "ANY VAR NAME" ANY STR VALUE\n'
5. Особые команды:
  '[GUI] "CLEAR" 1\n' - Очистка таблицы параметров системы
  '[GUI] "CONNECT" TEXT\n' - Вывод текста в поле статуса
  
6. Команды запуска проверок:
  '[CMD] NAME\n'

]]


GUI_TCP_PORT = 5455        -- default port


function GUI_Emul()
  return "ERR", ""
end



-- Для команд установки, в которых есть переменная состовляющая, сохраняем не полную команду, а ID группы команд 
function GUI_SendCmd(value) 
 mcLinkTx (GUI.Line, value)
end



--------------------------------------------------
-- вывод в лог 
-- TEXT\n (цвет по умолчанию)
-- [LOG] TEXT {color="red", propN="value", ...}
--------------------------------------------------
function GUI_LOG(v, color) 
 -- log("TX>"..v) 
 if color then
  GUI_SendCmd("[LOG] "..v..'{color="'..color..'"}\n')
 else
  GUI_SendCmd(v.."\n")
 end
 return nil
end

function GUI_CMD(cmd, value) 
 if ((cmd)and(value)) then
  log("CMD>"..cmd.." : "..value) 
  GUI_SendCmd('['..cmd..'] '..value.."\n")
  end
 return nil
end

function GUI_ViewValue(id, v) 
  if ((v)and(id)) then
 -- log("TM>"..id..":"..v) 
  GUI_SendCmd('[TM] "'..id..'" '..v.."\n")
  end
 return nil
end

function GUI_SpecCmd(id, v) 
  if ((v)and(id)) then
  log("GUI>"..id..":"..v) 
  GUI_SendCmd('[GUI] "'..id..'" '..v.."\n")
  end
 return nil
end

function GUI_ClearCmd() 
  GUI_SpecCmd("CLEAR", "1")
end

function GUI_Progress(value) 
  if (value) then
   GUI_SpecCmd("PROGRESS", value)
   end
end


-- Тест завершился [норма / ненорма]
function GUI_Done(result) 
  if (result == nil) then
    result = 1
    end
  GUI_SpecCmd("TESTDONE", result)
end


function GUI_Dialog(value, isWait, param) 
  GUI.DialogResult = nil
  
  if (value) then
    if (param) then
      -- если есть параметр диалога, добавим его к url:param
      value = value .. ":" .. param
      end
    GUI_SpecCmd("DIALOG", value)
    
    -- Если требуется ждать ответа (isWait != nil)
    if (isWait) then
      -- время ожидания не ограничено
      GUI.State.waitDialog = 1
      while (GUI.DialogResult == nil) do
        pause(1000*1000) -- 1c
        if (GUI.State.waitDialog == 0) then
          break
          end
      end   
    end
  end
  return GUI.DialogResult
end


function GUI_RunAlg(id, v, isWait) 
  GUI.AlgResult = nil
  if ((v)and(id)) then
    log("ALG>"..id..":"..v) 
    GUI_SendCmd('[ALG] "'..id..'" '..v.."\n")
    
    -- Если требуется ждать ответа (isWait != nil)
    if (isWait) then
      local waitCount = 0;
      -- работа алгоритма не должна превышать 10с
      while (GUI.AlgResult == nil) do
        pause(100*1000) -- 0.1c
        waitCount = waitCount + 1
        if (waitCount > 100) then
          log("Время ожидания истекло!")
          break
        end
      end
    end
  end
  return GUI.AlgResult
end

-- Обработка входных сообщений от GUI
function GUI_onEventCommand(cmd, value)
  -- бывает, что ядро скриптов остается в ожидании диалога, при том что GUI уже закрыто
  -- пусть, любая команда от GUI прерывает ожидание диалога
  if (GUI.State.waitDialog > 0) then
    GUI.State.waitDialog = 0 -- выход из ожидания!
  end
  
  log("GUI CMD> "..cmd.." --> "..value)
  -- "RUN" - Группа команд запуска/остановкой тестов
  if cmd == "RUN" then
    if value == "BREAK" then
      -- служебная команда : прервать проверку
      GUI.isBreak = 1
      if (GUI.OnBreak) then
        GUI.OnBreak()
        end
    else
      -- Запуск тестов
      if (GUI.OnRunTest) then
        GUI.OnRunTest(value)
        end
    end
  end
  -- "DIALOG" - Событие от окна диалога (команды от кнопок диалога)
  if cmd == "DIALOG" then
    log("GUI DIALOG> "..cmd.." --> "..value)
    GUI.DialogResult = value
    end
  -- "ALG" - результат алгоритма
  if cmd == "ALG" then
    log("ALG RESULT> value --> "..value)
    GUI.AlgResult = value
    end
  -- "CMD" - команды общего назначения
  if cmd == "CMD" then
    if (GUI.OnEvent) then
      GUI.OnEvent(cmd, value)
      end
  end
  
end

function GUI_Init(PORT)
  if (DriverLineId) then
    DriverLineId = DriverLineId + 1
  else
    DriverLineId = 1
  end

  GUI = {}
  GUI.Line    = DriverLineId
  -- Пользовательский обработчик события запуска проверки
  GUI.OnRunTest = nil
  -- [reserv]
  GUI.OnError = nil
  -- Пользовательский обработчик сообщений от GUI
  GUI.OnEvent = nil
  -- Пользовательский обработчик события прерывания проверки
  GUI.OnBreak = nil
  -- GUI.Log(): выдача сообщений в лог окна
  GUI.Log     = GUI_LOG
  -- GUI.Cmd(): выдача произвольных команд системе GUI
  GUI.Cmd     = GUI_CMD
  -- GUI.Clear(): выдача команды "CLEAR" системе GUI : переход в начальное состояние
  GUI.Clear   = GUI_ClearCmd
  -- GUI.View(): передача параметра для отображения в таблице параметров GUI
  GUI.View    = GUI_ViewValue
  -- GUI.Progress(): В ходе проверки индикация хода выполнения
  GUI.Progress= GUI_Progress
  -- GUI.Done(): Переход GUI к начальному окну (список тестов)
  GUI.Done    = GUI_Done
  -- GUI.Dialog(): Открыть новое окно/диалог. Опция1: Ждать / нет;  Опция2: передать строку в диалог
  GUI.Dialog  = GUI_Dialog
  -- GUI.RunAlg(): Запуск алгоритма (Взаимодействие с алгоритмом)
  GUI.RunAlg  = GUI_RunAlg
  -- GUI.DialogResult - переменная : результат диалога (строка)
  GUI.DialogResult = nil
  -- GUI.AlgResult - переменная : результат алгоритма (строка)
  GUI.AlgResult = nil
  -- GUI.isBreak - переменная : статус прерывания теста (не nil - прервано)
  GUI.isBreak = nil
  
  GUI.State = {LastCmd = "", LastValue = 0, waitDialog = 0}

  if (PORT) then -- nil --> use default
   GUI_TCP_PORT = PORT
  end
  
  log(string.format("GUIInit( [PORT:%d] )", GUI_TCP_PORT)) 

  addObject("LINE",   "LINEGUI",  GUI.Line, "[nUse]", GUILineProcTX)
  return GUI
end


function GUILineProcTX(idEvent, buffer, LineName)
if (idEvent == "INIT") then
 log("INIT") 
 mcLinkOpen (GUI.Line, "TCPSRV", "", GUI_TCP_PORT, "DUMMY")
end
if (idEvent == "RX") then
  log("GUI RX>"..buffer) 
  -- [CMD] ID -- "[CMD] EXIT\n"
  local cmd
  local value
  cmd, value = string.match(buffer, '^%[(%a+)%]%s*(.+)\n$')
  if cmd and value then
    GUI_onEventCommand(cmd, value)
    end
  end

return "OK", ""
end


