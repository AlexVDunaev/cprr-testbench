
REP_HEAD_PATTERN = [[<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title> Радар №$SN$ Тест : $TEST$</title>
  <style>
   @import url(dnvStyleSky.css);
  </style>
</head>
<body>
<h1>$TEST$</h1>
<table><tbody><tr><td>
 <img src="qr-code.gif" alt="RADAR SN: $SN$">
</td><td>
<h2>Радар №$SN$</h2>
<h2>Время испытаний: $TIME$</h2>
</td></tr></tbody></table>
]]

REP_BODY = ""

function REPORT_New(SN, ReportName)
  -- заполнение обязательных полей
  if (SN) then 
   REP_HEAD_PATTERN = string.gsub(REP_HEAD_PATTERN, "%$SN%$", SN)
   end
  if (ReportName) then 
   REP_HEAD_PATTERN = string.gsub(REP_HEAD_PATTERN, "%$TEST%$", ReportName)
   end
  REP_HEAD_PATTERN = string.gsub(REP_HEAD_PATTERN, "%$TIME%$", os.date("%Y-%m-%d %H-%M-%S"))
end

function REPORT_Add(text, ext, html)
  if ((text==nil)and(ext==nil)and(html)) then
    REP_BODY = REP_BODY .. html.."\n"
    return 
    end
  if (text==nil) then
    text = ""
    end
  if (ext) then
    REP_BODY = REP_BODY .. string.format("<%s>%s</%s>\n", ext, text, ext)
   else
    REP_BODY = REP_BODY .. string.format("<p>%s</p>\n", text)
   end
  log("REPORT>"..text)
end

function REPORT_Done()
  REP.fileName = "./reports/test.html"
  REP.file = io.open( REP.fileName, "w"); 
  REP.file:write(REP_HEAD_PATTERN);
  REP.file:write(REP_BODY);
  REP.file:write("</body></html>\n");
  REP.file:close();
  -- convert to PDF:
  -- os.execute("wkhtmltopdf.exe ./reports/test.html ./reports/test.pdf > 1.txt")
  REP_BODY = ""
end


function REPORT_Init()

  REP = {}
  REP.new       = REPORT_New
  REP.add       = REPORT_Add
  REP.done      = REPORT_Done
  REP.file      = nil
  REP.fileName  = nil
  return REP
end