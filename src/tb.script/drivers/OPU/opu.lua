--[[

РЭ: 192.168.0.111:80 TCP (Адрес ПК 192.168.0.2 – 192.168.0.100)
REAL:192.168.70.10:23 TCP 


POSx - x = [1 : Азимут | 2 : Поляризация]
IDN?\n --> EMS,Two-Axis Motion Stand, firmware version 1.3{0A}
EN1?\n --> 0{0A}
POS1?\n --> 0{0A}



]]

OPU_TCP_IP = "192.168.1.60" -- default IP
OPU_TCP_PORT = 8110        -- default port
OPU_TIMEOUT = 5000
OPU_TRY_NUM = 3


function OPU_Emul()
  return "ERR", ""
end


function OPU_reqCmd(minLength, regExpFilter, limitTimeOut)
  if (isEmul == 1) then
    return OPU_Emul()
    end
  buffer = ""
  stepTimeout = 50
  -- print("OPU_reqCmd(minLength : " .. minLength .. ")")
  while true do
    -- читаем TCP
    rxResult, bufferRx = mcLinkRx(OPU.Line, minLength, stepTimeout)
    
    if (rxResult == "OK") then
     buffer = buffer .. bufferRx
     if (string.match(buffer, regExpFilter)) then
       -- есть подходящий буфер
       -- print("OPU_reqCmd : [OK] -> ", buffer)
       return "OK", buffer
     end
     -- Нужного ответа пока нет, продолжаем запрашивать данные
    if (limitTimeOut <= stepTimeout) then
      -- общий таймаут вышел !
      print("TIMEOVER")
      return "ERR" , ""
    else
    -- print("NEXT")
    end
    limitTimeOut = limitTimeOut - stepTimeout
    -- print("limitTimeOut : " .. limitTimeOut)
     else
      print("LINE ERROR!")
      return "ERR" , ""
    end
    end
  end


-- Для команд установки, в которых есть переменная состовляющая, сохраняем не полную команду, а ID группы команд 
function OPU_SendCmd(cmd, cmdId, value) 
 OPU.State.LastCmd = cmd
 OPU.State.LastValue = value
 if (cmdId) then
   OPU.State.LastCmd = cmdId
 end
 mcLinkTx (OPU.Line, cmd .. "\n")
 --print("OPU>"..cmd)
end



-- Универсальная функция посылки команды [3 повтора] и ожидание ответа нужного формата. Генерация ошибки если не получен ответ
function OPU_Request(strCmd, idCmd, valueCmd, minLength, regExpFilter, limitTimeOut, regExpExtract, labelString) 
  local i;
  local v1;
  local v2;
-- три попытки запросить прибор
 for i = 1, OPU_TRY_NUM do 
   
   OPU_SendCmd(strCmd, idCmd, valueCmd)
   
   result, buffer = OPU_reqCmd(minLength, regExpFilter, limitTimeOut)
   
   if (result == "OK") then
      v1 = string.match(buffer, regExpExtract)
      if (v1 ~= nil) then
       return v1
      end
   end
  print(labelString .. " - ERROR!")
  end
  print(labelString .. " - FATAL ERROR!")
  if (OPU.OnError) then
    OPU.OnError(string.format("%s(%s)", labelString, valueCmd))
    end
  return nil
end


--------------------------------------------------
-- Запрос угла поворота
--------------------------------------------------
function OPU_GetAzimuth() 
 local cmdStr;
 local f;
 cmdStr = 'POS1?'
 f = OPU_Request(
         cmdStr,     -- команда
         "POS1?",    -- идентификатор команды (для эмулятора)
         0,          -- значение параметра (для эмулятора)
         #("0\n"),   -- минимальная длина ответа
         "%d+\n$",   -- regExp, описывающее SCPI ответ
         OPU_TIMEOUT,-- Предельное время ожидания ответа
         '(%d+)\n$', -- regExp, с захватами, для извлечения параметров ответа
         "POS?"      -- Строка-идентификатор, используемая при выдачи сообщения от ошибке
       )
  if (f) then
    OPU.State.AzPos = tonumber(f)
    return OPU.State.AzPos
    end
end

--------------------------------------------------
-- Запрос состояния [1 : Ошибка | 2 : Ноль найден (MH1) | 3 : закончил движение | 4: в движении]
--------------------------------------------------
function OPU_GetState() 
 local cmdStr;
 local f;
 cmdStr = 'STAT1?'
 f = OPU_Request(
         cmdStr,     -- команда
         "STAT1?",    -- идентификатор команды (для эмулятора)
         0,          -- значение параметра (для эмулятора)
         #("0\n"),   -- минимальная длина ответа
         "%d+\n$",   -- regExp, описывающее SCPI ответ
         OPU_TIMEOUT,-- Предельное время ожидания ответа
         '(%d+)\n$', -- regExp, с захватами, для извлечения параметров ответа
         "STAT1?"      -- Строка-идентификатор, используемая при выдачи сообщения от ошибке
       )
  if (f) then
    OPU.State.Status = tonumber(f)
    return OPU.State.Status
  end
  return nil
end

--------------------------------------------------
-- Запрос состояния [1 : нет питания | 2 : Не выполнен поиск нуля (MH1) | 3 : Задание за пределом]
--------------------------------------------------
function OPU_GetError() 
 local cmdStr;
 local f;
 cmdStr = 'ERR1?'
 f = OPU_Request(
         cmdStr,     -- команда
         "ERR1?",    -- идентификатор команды (для эмулятора)
         0,          -- значение параметра (для эмулятора)
         #("0\n"),   -- минимальная длина ответа
         "%d+\n$",   -- regExp, описывающее SCPI ответ
         OPU_TIMEOUT,-- Предельное время ожидания ответа
         '(%d+)\n$', -- regExp, с захватами, для извлечения параметров ответа
         "ERR1?"      -- Строка-идентификатор, используемая при выдачи сообщения от ошибке
       )
  if (f) then
    OPU.State.Error = tonumber(f)
    OPU.Send("CLR1") -- сброс ошибки после чтения
    return OPU.State.Error
  end
  return nil
end


--------------------------------------------------
-- Установка угла поворота
--------------------------------------------------
function OPU_SetAzimuth(v) 
 local cmdStr;
 local i
 local a
 local last_a
 v = v * 1000
 cmdStr = string.format('MOVE1 %d', v)
 OPU_SendCmd(cmdStr) 
 -- контроль состояния
 last_a = 777777
 for i = 0, 500 do
    pause(200*1000) -- 0.2c
    a = OPU_GetState()
    if (a) then
    --log(string.format("STATUS ON MOVE: [%d]", a)) 
      if (a == 3) then
        -- OK
        return v -- позиция установлена с достаточной точностью
        end
      if (a == 1) then
        -- ERROR
        a = OPU_GetError()
        if (a) then
        log(string.format("ERROR ON MOVE: [%d]", a)) 
         end
        return nil
        end
      if (a == 4) then
        -- MOVE
          a = OPU_GetAzimuth()
          if (a) then
            -- log(string.format("current pos: %d (err: %d)", a, math.abs(a - v))) 
            if (a == last_a) then
              log("NO MOVE?!...") 
              -- Завис?
              OPU.Send("CLR1")
              pause(500*1000) -- 0.5c
              return nil -- возврат с ошибкой!
            end
            last_a = a
          end
      end
    end  
  end
  -- TIMEOUT
 return nil
end


--------------------------------------------------
-- Установка угла поворота
--------------------------------------------------
function OPU_SetZero() 
 local cmdStr;
 local i
 local a
 OPU.Send("CLR1")
 pause(100*1000) -- 0.5c
 cmdStr = 'MH1'
 OPU_SendCmd(cmdStr) 
 -- контроль состояния
 for i = 0, 500 do
    pause(200*1000) -- 0.2c
    -- log(string.format("WAIT... ON ZERO: [%d]", i)) 
    a = OPU_GetState()
    if (a) then
    -- log(string.format("STATUS ON ZERO: [%d]", a)) 
      if (a == 2) then
        -- OK
        return 0 -- ноль найден
        end
      if (a == 1) then
        -- ERROR
        a = OPU_GetError()
        log(string.format("ERROR ON MOVE(ZERO): [%d]", a)) 
        return nil
        end
      if (a == 4) then
        -- MOVE... wait
      end
    end  
  end
  -- TIMEOUT
 log(string.format("TIMEOUT... ON ZERO")) 
 return nil
end

--------------------------------------------------
-- Команды возврата в исх. состояние
--------------------------------------------------
function OPU_DeInitOpu() 
  OPU.Send("CLR1")
  pause(500*1000) -- 0.5c

  OPU.Send("EN1 0")
  pause(500*1000) -- 0.5c

end

--------------------------------------------------
-- Команды начальной установки
--------------------------------------------------
function OPU_InitOpu() 
  local rxResult
  local bufferRx
  
  OPU.Send("IDN?")
  pause(500*1000) -- 0.5c
  rxResult, bufferRx = mcLinkRx(OPU.Line, 10, 5010)
  --rxResult = "OK"
  if (#bufferRx > 0) then
    log(string.format("OPU : %s", bufferRx)) 
  else
    return -1  
  end
  
  OPU.Send("CLR1")
  pause(500*1000) -- 0.5c

  OPU.Send("EN1 1")
  pause(500*1000) -- 0.5c
  
  OPU.Send("SPD1 6") -- 6 град/сек
  pause(500*1000) -- 0.5c
  
  return OPU_SetZero()
end



function OPU_Init(IP, PORT)
  log(string.format("OPUInit( [IP: %s:%d] )", IP, PORT)) 
  if (DriverLineId) then
    DriverLineId = DriverLineId + 1
  else
    DriverLineId = 1
  end

  OPU = {}
  OPU.Line    = DriverLineId
  OPU.OnError = nil
  OPU.SetAzimuth = OPU_SetAzimuth
  OPU.Init = OPU_InitOpu
  OPU.DeInit = OPU_DeInitOpu
  OPU.Send = OPU_SendCmd
  OPU.State = {LastCmd = "", LastValue = 0, Azimuth = 0, AzPos = 0, Status = 0, Error = 0}

  if (IP) then -- nil --> use default
   OPU_TCP_IP = IP
  end
  if (PORT) then -- nil --> use default
   OPU_TCP_PORT = PORT
  end

  addObject("LINE",   "LINETXOPU",  OPU.Line, "[nUse]", OPULineProcTX)
  return OPU
end


function OPULineProcTX(idEvent, buffer, LineName)
if (idEvent == "INIT") then
 mcLinkOpen (OPU.Line, "TCP", OPU_TCP_IP, tostring(OPU_TCP_PORT), "DUMMY")
 end
return "OK", ""
end


