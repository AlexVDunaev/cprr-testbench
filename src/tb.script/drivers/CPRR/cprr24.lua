

CPRR_UDP_IP = "192.168.1.60" -- default IP
CPRR_UDP_PORT = 7010         -- default port
CPRR_UDP_PORT_LOCAL = nil    -- default local port

CPRR_LastCmd = ""
CPRR_FrameCount = 0

function CPRR_SendCmd(cmd) 
 CPRR_LastCmd = cmd
-- log(string.format("CPRR> %s", cmd)) 
 mcLinkTx (CPRR.Line, cmd .. "\n")
end

-- os.date("%Y-%m-%d %H-%M-%S") - получение времени, если нужно создавать файл имя-дата

-- Установка режима радара на получение сырых данных, разных форматов
-- mode == "STREAM" : Штатная RF конфигурация. size - кол-во кадров, прием стрима
-- mode == "T25" : Штатная RF конфигурация. size - кол-во кадров, прием целей
-- mode == "TESTER" : Штатная RF конфигурация. size - кол-во кадров, прием спец. сообщений
-- mode == "ADC" : Частная RF конф. Непрерывный сбор данных. size - кол-во отсчетов АЦП.
function CPRR_GetRawData(mode, size) 
 log(string.format("GetRawData(%s)", mode)) 
 -- Перевод в режим стрима и запись в файл 100 кадров
 CPRR_FrameCount = 0
 result = mcLinkCtrl(CPRR.Line, "STREAM", "new.000.007.t25.bag", 2000)
 print("mcLinkCtrl():"..result)
 -------------------------------------------------------
end


function CPRR_SaveStream(filename, frameNum, mode) 
 -- Перевод в режим стрима и запись в файл frameNum кадров
 CPRR_FrameCount = 0
 if (mode) then
  result = mcLinkCtrl(CPRR.Line, "STREAM:"..mode, filename, frameNum)
else
  result = mcLinkCtrl(CPRR.Line, "STREAM", filename, frameNum)
  end
 log(string.format("SaveStream : write num = %d", result)) 
 return result
 -------------------------------------------------------
end

function CPRR_T25(frameNum) 
 -- Перевод в режим получения отметок
 result = mcLinkCtrl(CPRR.Line, "T25", "", frameNum)
 log(string.format("T25> frame num = %d", result)) 
 return result
 -------------------------------------------------------
end




function CPRR_Init(IP, PORT, PORT_LOCAL)
  if (DriverLineId) then
    DriverLineId = DriverLineId + 1
  else
    DriverLineId = 1
  end
  log(string.format("CPRRInit( [IP: %s:%d] )", IP, PORT)) 

  CPRR = {}
  CPRR.Line       = DriverLineId
  CPRR.GetRawData = CPRR_GetRawData
  CPRR.SaveStream = CPRR_SaveStream
  CPRR.GetTargets = CPRR_T25
  CPRR.CMD        = CPRR_SendCmd
  CPRR.TrackingEvent = nil
  CPRR.StreamEvent = nil
  

  if (IP) then -- nil --> use default
   CPRR_UDP_IP = IP
  end
  if (PORT) then -- nil --> use default
   CPRR_UDP_PORT = PORT
  end
  
  CPRR_UDP_PORT_LOCAL = PORT_LOCAL

  addObject("LINE",   "RADAR_CPRR",  CPRR.Line, "[nUse]", CPRRLineEvent)

  return CPRR
end


function CPRR_TestTargetDouble(tgList, GNumber)
      -- тест дублирования целей в списке целей
      local T = {}
      local i
      local t
      local id
      
      for i, t in pairs(tgList) do
--        id = string.format("ID%.4f_%.4f_%.6f", t.R, t.Az,  t.Rcs)
        id = string.format("ID%.4f_%.4f_%.6f", t.X, t.Y,  t.Rcs)
        if T[id] then
          T[id][0] = T[id][0] + 1
          T[id][T[id][0]] = t
          log(string.format("TRACK COPY DETECTED!!: %.4f_%.4f_%.6f <--> (%s) ID:%d --> %d", t.X, t.Y,  t.Rcs, id, t.id, T[id][0]))
        else
          T[id] = {}
          T[id][0] = 1
          T[id][1] = t
          --log(string.format("TG: ADD!!: --> "))
        end
      end
      for k, v in pairs(T) do
        if (v[0] > 1) then
          log(string.format("[FRAME:%d]: TRACK COPY NUM [%d]", GNumber, v[0]))
          for k2, v2 in pairs(v) do
            if (k2 > 0) then
             log(string.format("id: %d, X:%.5f, Y:%.5f, RCS:%.5f, vX:%.5f, vY:%.5f", v2.id, v2.X, v2.Y, v2.Rcs, v2.Xv, v2.Yv))
            end
          end
        end
      end

end

-- на вход получаем "склееный" пакет: заголовок + [цели пакет №1] + [цели пакет №2] + ... + [цели пакет №N]
function CPRR_GetTargetList(pkt)
  local result = {}
  local tgHeaderSize = 12 + 4 + 8*3
  local tgSize = 88
  local tgNum = (#pkt - tgHeaderSize) // tgSize
  local tgIndex = tgHeaderSize
  local i
  local ObjID, Range, Azimuth, LiveTime, Rcs, Xcoord, Xrate, Xacceleration, Ycoord, Yrate, Yacceleration

  for i = 1, tgNum do
    ObjID, Range, Azimuth, LiveTime, Rcs, Xcoord, Xrate, Xacceleration, Ycoord, Yrate, Yacceleration = 
       string.unpack("i8ddi8ddddddd", string.sub(pkt, tgIndex + 1, tgIndex + tgSize))
    result[i] = {id = ObjID, R = Range, Az = Azimuth, Time = LiveTime, Rcs = Rcs, X = Xcoord, Y = Ycoord, Xv = Xrate, Yv = Yrate, Xa = Xacceleration, Ya = Yacceleration}
    tgIndex = tgIndex + tgSize
    end
  return result, tgNum
  end



function CPRR_GetHashTab(tgList, hashFormat)
  local T = {}
  local i
  local t
  local id
  if (tgList) then
    for i, t in pairs(tgList) do
      id = string.format(hashFormat, t.R, t.Az*0, t.Yv*0,  t.Rcs)
      if T[id] then
        T[id] = T[id] + 1
      else
        T[id] = 0
      end
    end
   end
  return T
end

-- Сравнение списков (в режиме сырых детекций)
function CPRR_TargetCompare(tgList1, tgList2, rNum, aNum, dNum, AmpNum)
  local T1 = {}
  local T2 = {}
  local totalCount = 0 -- общее число отметок
  local hitCount = 0 -- кол-во совпадений
  local hashFormat = string.format("ID_%%.%df_%%.%df_%%.%df_%%.%df", rNum, aNum, dNum, AmpNum)
  
  T1 = CPRR_GetHashTab(tgList1, hashFormat)
  T2 = CPRR_GetHashTab(tgList2, hashFormat)
  -- в одну сторону
  for k, v in pairs(T1) do
    --print(">>>", k)
    totalCount = totalCount + 1
    if (T2[k]) then
      hitCount = hitCount + 1
      --print(">>>", k, " + ")
    else
      --print(">>>", k, " - ")
    end
  end
  -- в другую сторону
  for k, v in pairs(T2) do
--    print("<<<", k)
    totalCount = totalCount + 1
    if (T1[k]) then
      hitCount = hitCount + 1
      --print("<<<", k, " + ")
    else
      --print("<<<", k, " - ")
    end
  end
  return totalCount, hitCount
end


Stat = {}
Stat.maxTgNum = 0

function CPRRLineEvent(idEvent, buffer, LineName, ptrData, sizeOfData)
  --print("CPRRLineEvent: ", idEvent, LineName, #buffer)

if (idEvent == "INIT") then
  if (CPRR_UDP_PORT_LOCAL) then
    mcLinkOpen (CPRR.Line, "CPRR24", CPRR_UDP_IP, string.format("%d;%d", CPRR_UDP_PORT, CPRR_UDP_PORT_LOCAL), "DUMMY")
  else
    mcLinkOpen (CPRR.Line, "CPRR24", CPRR_UDP_IP, string.format("%d", CPRR_UDP_PORT), "DUMMY")
  end
end
--  "RX" EVENT (STREAM MODE): buffer : [  "" | HEADER ]
--   buffer = "" : TIMEOUT или ERROR
--   buffer = HEADER - заголовок кадра.
--   return: "OK", "" - действия по умолчанию (запись кадра)
--   return: "OK", buffer - возможность изменить содержимое заголовока перед записью
--   return: "IGNORE", "" - игнорировать кадр
--   return: "BREAK", "" -  прервать запись стиминга
if (idEvent == "RX") then
  if (buffer == "") then
    -- CPRR DRIVER> TIMEOUT или ERROR
    return "BREAK", "" --> STREAMING EXIT
    end
  
  header = buffer
  --hexlog("header>", header)
  local stx
  local len
  local cmd
  local time
  local GNumber
  local t1, t2, t3, t4, t5, t6

  if (#header >= 12) then 
    stx, len, cmd = string.unpack("i4i4i4", header)
    if (cmd == 10) then 
      if (#header >= 52) then 
        stx, len, cmd, time, GNumber, t1, t2, t3, t4, t5, t6 = string.unpack("i4i4i4i8i8i2i2i2i2i2i2", header)
      
      if (CPRR.StreamEvent) then
        local resultStatus
        local resultAction
        resultStatus, resultAction = CPRR.StreamEvent(time, GNumber, t1, t2, t3, t4, t5, t6, ptrData, sizeOfData)
        if (resultStatus) then
          return resultStatus, ""
          end
      end
      
      else
        log(string.format("CPRRLineEvent>[RX] : header size: %d", #header)) 
      end
      CPRR_FrameCount = CPRR_FrameCount + 1
    end
    if (cmd == 5) then 
      stx, len, cmd, HardVerLo, HardVerHi, SoftVerLo, SoftVerHi, RadarSN = string.unpack("i4i4i4i2i2i2i2i4", header)
      log(string.format("CPRRLineEvent>[RX] : [INFO] HW: %d.%d SW: %d.%d SN: %d", HardVerHi, HardVerLo, SoftVerHi, SoftVerLo, RadarSN)) 
      end
    
    if (cmd == 4) then 
      -- targets data
      -- log(string.format("CPRRLineEvent>[RX] : targets data (size: %d)", #buffer)) 
      stx, len, cmd, status, GNumber, timeStamp, speed = string.unpack("i4i4i4i4i8i8d", header)
      local tgNum, tgList
      tgList, tgNum = CPRR_GetTargetList(buffer)
      
      if (CPRR.TrackingEvent) then
        CPRR.TrackingEvent(status, GNumber, timeStamp, speed, tgNum, tgList)
      end
      
    end
  else
  log(string.format("CPRRLineEvent>[RX] : header size: %d", #header)) 
  end
  
  return "OK", ""
 end

return "OK", ""
end
