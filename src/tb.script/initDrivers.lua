dofile("./tb.script/drivers/N5770/N5770.lua")
dofile("./tb.script/drivers/CPRR/cprr24.lua")
dofile("./tb.script/drivers/N5183B/N5183B.lua")
dofile("./tb.script/drivers/N9040B/N9040B.lua")
dofile("./tb.script/drivers/OPU/opu.lua")
dofile("./tb.script/drivers/GUI/GUI.lua")
dofile("./tb.script/drivers/REPORT/report.lua")
-------------------------------------------------

isDebug = 0
isEmul  = 0
cmdIP = "192.168.1.4"
cmdPort = 7000
RadarSN = "0"

function InitDrivers()

  REPORT = REPORT_Init()
  
  GUI = GUI_Init() 

  cmdIP = mcGetParam("args", "IP", "192.168.1.4")
  log(string.format("---------------> IP is: %s", cmdIP))

  cmdPort = mcGetParam("args", "PORT", "7000")
  log(string.format("---------------> PORT is: %d", cmdPort))


  if isDebug == 1 then
--   OPU = OPU_Init("192.168.1.60", 8110) 
   
   RADAR = CPRR_Init("127.0.0.1", 7000, 7001) 
-- RADAR = CPRR_Init("127.0.0.1", 7100, 7001) 
-- RADAR = CPRR_Init("192.168.1.6", 7006, 7001) 
   
   POWER = N5770_Init("192.168.1.222", 5025)

   --RF_GEN = N5183B_Init("192.168.1.100", 5025)

   --SPECTRUM = N9040B_Init("192.168.1.60", 5025)

  else

   -- OPU = OPU_Init("192.168.70.10", 23) 
   
   RADAR = CPRR_Init(cmdIP, cmdPort)

   --POWER = N5770_Init("192.168.1.222", 5025)

   RF_GEN = N5183B_Init("192.168.1.100", 5025)

   --SPECTRUM = N9040B_Init("192.168.1.40", 5025)
  end

  log("InitDrivers> done!")
end


