
-- Определим обработчик ошибки ()
function FatalError(code)
  log(string.format("FatalError> code : %s", code))
  -- аварийное приведение в исх. состояние
  mcFunc("EXIT", 1)
  end


function TEST_SPECTRUM()
  SPECTRUM.OnError = FatalError
  
  mode = SPECTRUM.SetMode("RTSA")
  print("Mode: ", mode)
  pause(1000*1000) -- 1.0c
  Freq = 24E9
  SPECTRUM.SetFreq(Freq)
  print("SET FREQ: 24GHz")
  SPECTRUM.SetSpan() -- FULL SPAN
  print("SET FULL SPAN")
  pause(1000*1000) -- 1.0c
  print("START CHECK...")
  for i = 1, 20 do
    Freq = Freq + 5E6
    f, dB = SPECTRUM.SetMarker(Freq)
    if (dB > -999) then
      -- -1000.00dB - Error level
      print(string.format("STEP+:%d> FREQ[%.4fGHz]:%.2fdB", i, f/1E9, dB))
      end
    pause(500*1000) -- 0.5c
  end
  for i = 1, 20 do
    Freq = Freq - 5E6
    f, dB = SPECTRUM.SetMarker(Freq)
    print(string.format("STEP-:%d> FREQ[%.4fGHz]:%.2fdB", i, f/1E9, dB))
    pause(500*1000) -- 0.5c
  end
  SPECTRUM.PrintScreen("Test1.png")
  SPECTRUM.SaveData("Test1.csv")
 end

function TEST_POWER()
 Voltage = 0.0
  POWER.SetVoltage(0.0)
  POWER.SetCurrent(1)
  pause(2000*1000) -- 1c
  POWER.SetOutput(1)
  for i = 1, 2 do
    Voltage = Voltage + 0.5
    POWER.SetVoltage(Voltage)
    I, V = POWER.GetMeasure(FatalError, "POWER")
    print("POWER:", I, V)
    pause(200*1000) -- 1c
  end
  POWER.SetVoltage(0.0)
  POWER.SetOutput(0)
end

function TEST_RF_GEN()
  Freq = 24E9
  dBm  = -130
  RF_GEN.OnError = FatalError
  RF_GEN.SetPower(-130)
  for i = 1, 20 do
    Freq = Freq + 5E6
    dBm  = dBm  + 1
    if (dBm > -120) then
      dBm = -130
      end
    f = RF_GEN.SetFreq(Freq)
    RF_GEN.SetPower(dBm)
    print(string.format("STEP+:%d> FREQ[%.4fGHz]", i, f/1E9))
    pause(500*1000) -- 0.5c
  end
end

function TEST_RF_GEN()
  RADAR.SaveStream(string.format("new.000.%.3d.t25.bag", 0), 10)
end

function TEST_OPU()
  local i
  local j
  local Azimuth
  if (OPU.Init() >= 0) then
    log("OPU> ------- START --------")
    -- Установим начальную позицию.
    for j = 1, 5 do 
    OPU.SetAzimuth(0)
    log("OPU> Azimuth(0)")
    for i = 1, 10 do
     pause(100*1000) -- 0.5c
     Azimuth = i*5
     log(string.format("OPU> Azimuth : %d", Azimuth))
     OPU.SetAzimuth(Azimuth)
    end
   end
   OPU.DeInit()
  else
  log("OPU> ------- ERROR  --------")
    
  end
end


function TEST_OPU_AND_CPRR()
  local i
  local n
  local Azimuth
  if (OPU.Init() >= 0) then
    log("OPU> ------- START --------")
    -- Установим начальную позицию.
    OPU.SetAzimuth(0)
    log("OPU> Azimuth(0)")
    for i = 1, 10 do
     pause(100*1000) -- 0.5c
     Azimuth = i*5
     log(string.format("OPU> Azimuth : %d", Azimuth))
     OPU.SetAzimuth(Azimuth)
     n = RADAR.SaveStream(string.format("new.000.%.3d.t25.bag", i), 10)     
     if (n) then
      log(string.format("WRITE %d frames", n))
      end
    end
   OPU.DeInit()
  else
  log("OPU> ------- ERROR  --------")
    
  end
end


function TEST_GUI()
   for i = 1, 100 do
     GUI.Log("TEST LOG"..i)
     GUI.Log("TEST LOG(!)"..i)
     pause(1000*1000) -- 0.5c
     end
end

function TEST_RADAR_TARGET()

  GUI.Log("RADAR>T25 RUN...")
  --RADAR.GetTargets(10)
  RADAR.SaveStream("config.bag", 1, 4)
  
  

end


function TEST_MODULE_DEMO()
  local i
  -- HTML Цвета (https://colorscheme.ru)
  local colors = {"#000000", "Red", "Lime", "Crimson", "Green", "Coral", "Teal", "Gold", "Aqua", "SkyBlue", "Blue", "Brown", "Gray", "Black"};
  GUI.Clear()
  GUI.Progress(0)
  for i = 1, 10 do
   GUI.View("Какой-то парам.№"..i, "0."..i)
   end
   for i = 1, 10 do
     GUI.Progress(i*10)
     GUI.Log("RADAR>TEST LOG"..i, colors[i])
     pause(1000*1000) -- 1.0c
     GUI.View("Генератор, МГц", i*1111.1)
     if (BREAK) then 
       break;
      end
     end
   GUI.View("Результат", "НОРМА")
   GUI.Log("RADAR>DONE")
   GUI.Done(1)

end


function TEST_RADAR_TARGET_xN()
  
  log("RADAR>T25 test run...")
  for i = 1, 10 do
    log("RADAR> #"..i)
    RADAR.GetTargets(10)
    end
  log("RADAR>T25 test done...")
end



