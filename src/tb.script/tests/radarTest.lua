

gCnt_Frame = 0
gGrab_prev = 0

flag_once = 0
flag_radar_info = 0

cmdOUTPUTFILES = ""
gRadar_temperature = 0

tgListRadar = {}
tgListModel = {}

-- тест дублирования целей в списке целей
--CPRR_TestTargetDouble(tg, GNumber)
-----------------------------------------
function RadarTelnetParse(logfile)
  local a
  local f
  local info
  local content

  f = io.open(logfile,"r"); 
  content = f:read("*all")
  f:close();

  info = {}

  -- Для удобства восприятия удалю esc-последовательности:
  a = string.gsub(content, "\27%[%d*.", "")
  print(a)

  info.Hard = string.match(a, 'Hard:[.%s]*(%d+%.%d+)')
  info.Soft = string.match(a, 'Soft:[.%s]*(%d+%.%d+)')
  info.SN   = string.match(a, 'Serial Num:[.%s]*(%d+)')
  info.ConfigCode  = string.match(a, 'Config code:[.%s]*(0x[0123456789ABCDEF]+)')
  info.SavedCount = string.match(a, 'Saved count:[.%s]*(%d+)')
  info.Tracking = string.match(a, 'Tracking version:[.%s]*(%d+)')
  info.RAWMODE = string.match(a, '(RAW MODE ON)')
  return info
end


function userTrackingEvent(status, GNumber, timeStamp, speed, tgNum, tgList)
  -- targets data
  local i
  local fps = 0
  if (cprrLastTimeStamp) then
    fps = 1000000 / (timeStamp - cprrLastTimeStamp)
  end
  cprrLastTimeStamp = timeStamp
  if (cnt == nil) then 
    cnt = 0 
  end
  cnt = cnt + 1
  tgListRadar[GNumber] = tgList
  log(string.format("[%d][%d]: STAT: NUM:%d", cnt, GNumber, tgNum)) 

  local f_info = io.open(string.format("%s/GN%03d_%03d_R.txt", cmdOUTPUTFILES, GNumber, cnt),"w"); 
  local i, t
  for i, t in pairs(tgList) do
   --f_info:write(string.format("%d \t%10.6f \t%10.6f \t%10.5f \t%10.5f \t%11.9f\n", t.id, t.X, t.Xv, t.Y, t.Yv, t.Rcs)) --
   t.Az = math.pi/180.0 * t.Az
   f_info:write(string.format("%12.9f\t%12.9f\t%12.9f\t%12.9f\n", t.R, t.Az, t.Yv, t.Rcs)) --
  end
  -- Сохраняет изменения в файле
  f_info:close();
    
        
end


function userStreamEvent(time, GNumber, t1, t2, t3, t4, t5, t6)
  
  if (firstGNumber == nil) then
    firstGNumber = GNumber
    end
  gCnt_Frame = gCnt_Frame + 1

  local fps = 0
  if (cprrLastTimeStamp) then
     fps = 1000000 / (time - cprrLastTimeStamp)
  end
  cprrLastTimeStamp = time
  
  log(string.format("CPRRLineEvent>[RX][%04d] : GNumber: %4d fps: %.3f [%d %d %d %d %d %d]", gCnt_Frame, GNumber, fps, t1, t2, t3, t4, t5, t6)) 
  
  gRadar_temperature = t1
  
  local f1_info = io.open(string.format("%s/steam_check.txt", cmdOUTPUTFILES),"a"); 
  if (flag_radar_info == 1) then    
    f1_info:write(string.format("%d\n", GNumber)) --
  else
    f1_info:write(string.format("SN: , IP: %s, PORT: %s\n", cmdIP, cmdPort)) --
    f1_info:write(string.format("%d\n", GNumber)) --
    --log(string.format("--------------------------> IT IS HAPPEND")) 
  end
  
  flag_radar_info = 1
  --f1_info:write(string.format("%d\n", GNumber)) --
  f1_info:close();
  
end

function switch_to_raw_mode()

  local nTry
  for nTry = 1,3 do
    log(" SWITCH TO RAW MODE...")
    os.execute("plink -telnet "..cmdIP.."< telnet_cmd_tr_off.txt > out.txt")
    pause(5000*1000)
    a = RadarTelnetParse("out.txt")
    print(a.Hard, a.Soft, a.SN, a.ConfigCode, a.SavedCount, a.Tracking, a.RAWMODE)
    if (a.RAWMODE) then
     log(" SWITCH TO RAW MODE DONE")
     -- сохраняю параметры version радара
     if ((a.Hard)and(a.Soft)and(a.Tracking)and(a.ConfigCode))then
      REPORT.add("Информация о версии", "h3") -- формат заголовка
      REPORT.add("Версия аппаратной части: " .. a.Hard)
      REPORT.add("Версия прошивки: " .. a.Soft)
      REPORT.add("Версия трекинга: " .. a.Tracking)
      REPORT.add("Хэш-код конфигурации: " .. a.ConfigCode)
      break
      end
     else
       log(" SWITCH TO RAW MODE ERROR!!!!")
       pause(2000*1000)
       if (nTry == 3) then
         mcFunc("EXIT",1)  
         end
     end
   end
end

-- Режим стриминга
function STREAM_MODE()
  cmdFRAMENUM = mcGetParam("args", "FRAMENUM", "100")
  log(string.format("---------------> FRAMENUM is: %d", cmdFRAMENUM))
  
  cmdMODE = mcGetParam("args", "MODE", "2")
  log(string.format("---------------> MODE is: %s", cmdMODE))
  
  cmdOUTPUTFILES = mcGetParam("args", "OUTPUTFILES", "./123")
  log(string.format("---------------> OUTPUTFILES is: %s", cmdOUTPUTFILES))
  
  log(string.format("---------------> START STREAM MODE"))
  
  path_bag = string.format("%s/new.000.%.3d.t25.bag", cmdOUTPUTFILES, 0)
  
  RADAR.StreamEvent = userStreamEvent;
  
  -- 2 - штатный стрим
  -- 3 - стрим из истории
  n = RADAR.SaveStream(path_bag, cmdFRAMENUM, cmdMODE)  
  
  RADAR.StreamEvent = nil;
  log(string.format("---------------> END STREAM MODE"))
end

-- Особый режим стриминга
function STREAM_SP1_MODE()
  cmdFRAMENUM = mcGetParam("args", "FRAMENUM", "10")
  log(string.format("---------------> FRAMENUM is: %d", cmdFRAMENUM))
  
  cmdMODE = mcGetParam("args", "MODE", "5")
  log(string.format("---------------> MODE is: %s", cmdMODE))
  
  cmdOUTPUTFILES = mcGetParam("args", "OUTPUTFILES", "./123")
  log(string.format("---------------> OUTPUTFILES is: %s", cmdOUTPUTFILES))
  
  log(string.format("---------------> START STREAM MODE"))
  
  path_bag = string.format("%s/new_sp_.000.%.3d.t25.bag", cmdOUTPUTFILES, 0)
  
  RADAR.StreamEvent = userStreamEvent;
  -- 0 - idle режим
  -- 1 - режим трекинга
  -- 2 - штатный стрим
  -- 3 - стрим из истории
  -- 4 - передача по стриму настроек
  -- 5 - стрим изменённый
  n = RADAR.SaveStream(path_bag, cmdFRAMENUM, cmdMODE)  
  
  RADAR.StreamEvent = nil;
  log(string.format("---------------> END STREAM MODE"))
end

-- Сравнение алгоритма в радаре и модели
function TEST_RADAR_ALG_CMP()
  
  cmdFRAMENUM = mcGetParam("args", "FRAMENUM", "10")
  log(string.format("---------------> FRAMENUM is: %d", cmdFRAMENUM))
  
  cmdMODE = mcGetParam("args", "MODE", "3")
  log(string.format("---------------> MODE is: %s", cmdMODE))
  
  cmdOUTPUTFILES = mcGetParam("args", "OUTPUTFILES", "./123")
  log(string.format("---------------> OUTPUTFILES is: %s", cmdOUTPUTFILES))
  
  path_bag = string.format("%s/new.000.%.3d.t25.bag", cmdOUTPUTFILES, 0)

  writeIsOK = 1
  
  for tryCount = 1, 3 do 
   writeIsOK = RADAR.SaveStream(cmdOUTPUTFILES.."/config.bag", 1, 4)   
   print("writeIsOK : ", writeIsOK)
   if (writeIsOK > 0) then
     break
     end
  end
  if (writeIsOK == 0) then
   log("CONFIG READ ERROR!!!")
   mcFunc("EXIT",1)  
   end

  RADAR.TrackingEvent = userTrackingEvent;
  RADAR.StreamEvent = userStreamEvent;
 
  log(string.format("---------------> TRACKING MODE ON"))
  print(cmdFRAMENUM)
  n = RADAR.GetTargets(cmdFRAMENUM)
  
  log(string.format("---------------> HISTORY MODE ON"))
  n = RADAR.SaveStream("+"..path_bag, cmdFRAMENUM, 3)   
    
  log("-------------------------------- TEST----------------------------------")
  alg = Algorithm_Init("./config");
  --  alg.updateConfig("./config")
  
  cfg = alg.getConfig(cmdOUTPUTFILES.."/config.bag")
  print(cfg)
  if (cfg) then
    alg.setConfig(cfg, gRadar_temperature)
  end

  dTimeFrame = 0
  lastTime0 = nil
  u = nil
  for  frameIndex = 0, cmdFRAMENUM-1 do 
   --if (u == nil) then
    u, fdata = alg.getFrame(path_bag, frameIndex);
    --end
   if (fdata == nil) then
     break
     end
   if (lastTime0) then
    dTimeFrame = fdata.time - lastTime0
   end
   lastTime0 = fdata.time
   if (dTimeFrame == 0) then
     dTimeFrame = 0.05
     end
   if (u) then 
    local n = alg.calcFrame(u, dTimeFrame, 1) -- 1 сырые детекции, 0 рабочий режим трекинга
    local fName_Export = string.format("%s/GN%03d_%03d_M.txt", cmdOUTPUTFILES, frameIndex + firstGNumber, (frameIndex + 1))
    f = io.open(fName_Export,"w"); 
    if (n) then
     tgList = CPRR_GetTargetList(n);
     tgListModel[frameIndex + firstGNumber] = tgList
     local i, t
     for i, t in pairs(tgList) do
      e = string.format("%12.9f\t%12.9f\t%12.9f\t%12.9f\n", t.R, t.Az*3.141592/180.0, t.Yv, t.Rcs); -- print(e)
      f:write(e);
     end
    end
   f:close();
   end
  end
  
  if (n) then
      log(string.format("WRITE %d frames", n))
  end
  
  RADAR.TrackingEvent = nil;
  RADAR.StreamEvent = nil;
  log("------- Comparison of targets ------------")
  local P = GetRadarPower()
  if (P) then
    REPORT.add("Мощность потребления радара", "h3") -- формат заголовка
    REPORT.add(string.format("Мощность: %.1f Вт", P))
    end
  REPORT.add()
  REPORT.add("Сравнение результатов расчета алгоритма радара", "h3") -- формат заголовка
  REPORT.add("Файл: "..path_bag)

  local result = 1
    for frameIndex = 0, cmdFRAMENUM - 1 do 
    --for frameIndex = 5, 5 do 
        local hitResult
        local gNumber = firstGNumber + frameIndex
        if (tgListModel[gNumber] and tgListRadar[gNumber]) then 
        
          --totalCount, hitCount = CPRR_TargetCompare(tgListModel[gNumber], tgListRadar[gNumber], 4, 4, 4, 6)
    if (frameIndex == 5)  then
     local fName_Export = string.format("model_%d.txt", (frameIndex + 1))
     f = io.open(fName_Export,"w"); 
     for i, t in pairs(tgListModel[gNumber]) do
      e = string.format("%12.9f\t%12.9f\t%12.9f\t%12.9f\n", t.R, t.Az*3.141592/180.0, t.Yv, t.Rcs); -- print(e)
      f:write(e);
     end
    f:close();
    
     local fName_Export = string.format("radar_%d.txt", (frameIndex + 1))
     f = io.open(fName_Export,"w"); 
     for i, t in pairs(tgListRadar[gNumber]) do
      e = string.format("%12.9f\t%12.9f\t%12.9f\t%12.9f\n", t.R, t.Az, t.Yv, t.Rcs); -- print(e)
      f:write(e);
     end
    f:close();
    end
    
    
        totalCount, hitCount = CPRR_TargetCompare(tgListModel[gNumber], tgListRadar[gNumber], 4, 2, 2, 6)
          if (totalCount == 0) then
            hitResult = 100.0 -- Если целей нет в радаре и модели - совпадение 100%
           else
            hitResult = 100.0*hitCount/totalCount
          end
          if (hitResult < 90) then
            result = 0 -- error!
          end
          log(string.format("FRAME[%d, GN:%d] Total: %d, match: %d (%.1f%%)", frameIndex, gNumber, totalCount, hitCount, hitResult))
          REPORT.add(string.format("Кадр данных GN:%d, кол-во целей:%d, совпадения: %.1f%%", gNumber, totalCount, hitResult))
        else
          log(string.format("MODEL[GN:%d]  and RADAR[GN:%d] - empty", gNumber, gNumber + shift))
        end
      end
      
    tgListRadar = {}
    tgListModel = {}  
    firstGNumber = nil
    cnt = nil
    if (result == 0) then
  mcFunc("EXIT",1)  
  end
  return result
  --mcFunc("EXIT",1)  
  
end





