/* 
 * File:   log.h
 * Author: Dunaev
 *
 * Created on 15 Октябрь 2014 г., 14:50
 */
#include <fcntl.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/types.h>

#ifndef LOG_H
#define	LOG_H

#define	LOG_ID_NUM  100


#define	LEVEL_TITLE	127 // для вывода сообщений при загрузке сервера 
#define	LEVEL_ERR	3
#define	LEVEL_WARNING	2
#define	LEVEL_INFO	1
#define	LEVEL_DEBUG	0

#ifndef __VALIST
#ifdef __GNUC__
#define __VALIST va_list
//__gnuc_va_list
#else
#define __VALIST char*
#endif
#endif



typedef struct //
{
	char                LogID[20];
	FILE *              f;
} TLogData;

void    setLogLevel(int lvl);
char *  Log_write(char * text); // лог по умолчанию 
char *  Log_writeEx(const char * ID, char LEVEL, char * text, __VALIST params); // лог по умолчанию 
char    Log_Close() ;
void    Log_DBG(const char * text, ...);
void    Log_Ex(const char * ID, char LEVEL, char * text, ...);
void    Log_printbuf(char * title, unsigned char *d,int size);
char *  Log_buf2str(unsigned char *d,int size);
char *  Log_buf2strEx(unsigned char *str, int strSize, unsigned char *buff, int size);
void    Log_INIT(const char * text, ...);

int getLogLevel();


#endif	/* LOG_H */

