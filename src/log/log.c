#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include "log.h"
#include "../core/other.h"

#ifdef USE_SYS_LOG
 #include <syslog.h> // возможно использовать syslog...
#else
 #define closelog()
 #define openlog(...)
 #define syslog(...)
 #define	LOG_EMERG	0
 #define	LOG_ALERT	1
 #define	LOG_CRIT	2
 #define	LOG_ERR		3
 #define	LOG_WARNING	4
 #define	LOG_NOTICE	5
 #define	LOG_INFO	6
 #define	LOG_DEBUG	7

#endif



// <syslog.h> : openlog()
//            : syslog()
//            : closelog()
// При использовании функции syslog() лог будет дополняться в файл /var/log/syslog
// для настройки логирования (вывода сообщений в отдельный файл) /etc/syslog.conf (rsyslog)
// например: user.*   /home/log.log 
// --------------------
// Запись лога: файл log.log в Cygwin находится в C:\Tools\Cygwin\bin\

TLogData    LogData[LOG_ID_NUM];
int         LogDataUse = 0; // кол-во открытых лог-файлов
int         LogLevel = 0;
char        LogTmpBuff[1255] = "";
static 
int         writeToFile = 1; // 


//задание общего уровня логов
void setLogLevel(int lvl)
{
   LogLevel = lvl;
}

int getLogLevel() {
   return LogLevel;
}

//                sprintf((char *)outBuffer, "ERROR! {ID%d}", C->ID);
void  Log_INIT(const char * text, ...)
{
    va_list args;
    va_start(args, text);
    Log_writeEx("init", LEVEL_INFO, (char *)text, args);
    va_end(args);
}

void  Log_DBG(const char * text, ...) // лог по умолчанию (замена printf)
{
    va_list args;
    va_start(args, text);
    Log_writeEx("dbg", LEVEL_DEBUG, (char *)text, args);
    va_end(args);
  
}

void  Log_ERR(const char * text, ...)
{
    va_list args;
    va_start(args, text);
    Log_writeEx("err", LEVEL_ERR, (char *)text, args);
    va_end(args);
  
}

void  Log_Ex(const char * ID, char LEVEL, char * text, ...) //
{
    va_list args;
    va_start(args, text);
    Log_writeEx(ID, LEVEL, text, args);
    va_end(args);
}

char *  Log_write(char * text) // лог по умолчанию 
{
    if (writeToFile == 0) return text;
    //int f;
    FILE * f;
    //f = open("log.log", O_WRONLY | O_CREAT | O_EXCL, 0x0600);
    if (text)
        {
        f = fopen("log.log", "w"); //"a" - оставить старый файл
        if (f == 0) {
             printf("ERROR OPEN LOG FILE!\n");
             writeToFile = 0;
        } else {
            fprintf(f, "%s", text);
            fclose(f);
        }
        // запись в лог-файл по умолчанию
        //openlog("aprst.server", LOG_CONS | LOG_NDELAY | LOG_PID | LOG_LOCAL1, LOG_USER);
        //syslog(LOG_INFO, "%s", text);
        //syslog(LOG_WARNING, "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
        //closelog();
        }
    
    
    return text;
}


char *  Log_writeEx(const char * ID, char LEVEL, char * text, __VALIST params) // лог по умолчанию 
{

    //va_list args;
    char    logstr[8055]="";      // строка сообщения "TEXT..."
    char    time_logstr[855]=""; // строка сообщения с меткой времени "17.10 14:18.696>TEXT..."
    char    timestr[50]="";      // буфер для метки времени "17.10 14:18.696"
    char    fileName[255];

    FILE * f;
    int i,j;
    j = -1;
    
  //if (LogLevel > LEVEL) return text;
    if (writeToFile) {
  // поиск идентификатора лог - файла
  for (i = 0; i < LogDataUse; i++ )
    {
     if (strcmp(LogData[i].LogID, ID) == 0)
      {
      j = i;
      break;
      }
    }
  if (j == -1)
    { // файла пока не открыт
      if (LogDataUse < LOG_ID_NUM)
      {
      //mkdir("logs", 0766); // создадим директорию "logs"
      snprintf(fileName, sizeof(fileName), "logs/%s.log", ID);
      f = fopen(fileName, "w"); //"a" - оставить старый файл
        if (f == 0) {
         printf("ERROR OPEN LOG FILE! [%s]\n", fileName);
         writeToFile = 0;
        }
        else
        {
            j = LogDataUse;
            LogData[LogDataUse].f = f;
            strcpy(LogData[LogDataUse].LogID, ID);
            LogDataUse++;
        }
      }
      else
        {
         printf("ERR! TOO MANY LOG FILE!\n");
         return text;
        }
    }
    }
  // Соберем строку ...
  vsprintf(logstr, text, params);
  // произведем конвертацию символов
  //convertStr((unsigned char *)&logstr[0]);
  // приведем типы уровней в соответствие syslog
  if (LEVEL == LEVEL_DEBUG)   i = LOG_DEBUG;
  if (LEVEL == LEVEL_INFO)    i = LOG_INFO;
  if (LEVEL == LEVEL_WARNING) i = LOG_WARNING;
  if (LEVEL == LEVEL_ERR)     i = LOG_ERR;
  if (LEVEL > LEVEL_DEBUG)
   syslog(i, "%s", logstr); // сообщения уровня "DEBUG" писать не будем
  
  // добавим метку времени...
  mcGetTimeStr(timestr);
  if (strlen(logstr) >= sizeof(time_logstr))
      sprintf(logstr, "Error. Log buffer overflow. log size = %d", strlen(logstr));
  snprintf(time_logstr, sizeof(time_logstr), "%s> %s", timestr, logstr); // метка времени
  
  // запись в файл
  // проверим, если в конце строки нет перевода строки (\n) по добавим его
  i = strlen(time_logstr);
  // NB! При использовании функций printf/fprintf для корректного отображения 
  // символов русских букв, строку нужно передавать в параметре "...": printf("%s", str). 
  if (time_logstr[i - 1] != '\n')
  {
   //snprintf(time_logstr, sizeof(time_logstr), "%s\n", time_logstr); // для файла добавим перевод строки.
     time_logstr[ i ] = '\n';
     time_logstr[ i  + 1] = 0;
  }
  if (writeToFile) {
    fprintf(LogData[j].f, "%s", time_logstr);
    fflush( LogData[j].f);
  }
  // вывод на экран
  //if (LEVEL != LEVEL_INFO) // что бы не показывал лог обмена.
  if (LEVEL >= LogLevel)
   printf("%s",time_logstr);
}


char Log_Close() 
{
  if (writeToFile) {
    int i;
    for (i = 0; i < LogDataUse; i++ )
    {
      if (LogData[i].f)  
        fclose(LogData[i].f);
    }
  closelog();
  }
}


char * Log_buf2str(unsigned char *d,int size)
{
    // заполняем глобальный буфер и возврашаем на него указатель
    // нельзя использовать в многопоточном режиме!
return Log_buf2strEx(LogTmpBuff, sizeof(LogTmpBuff), d, size);
}

char * Log_buf2strEx(unsigned char *str, int strSize, unsigned char *buff, int size)
{
	#define RX_STR_LENGTH	160
	int	 i;

str[0] = 0;
strSize = strSize - 10; // буфер уменьшим для размещения строки "%s [%d]"
for(i = 0; i < size; i++)  
 if (3 * (i + 1) < RX_STR_LENGTH)
  snprintf(&str[strlen(str)], strSize, " %.2X",  buff[i]);
   else
  {
  snprintf(&str[strlen(str)], strSize, " [%d]",  size - i);
  break;
  }
return str;
 
}

void Log_printbuf(char * title, unsigned char *d,int size)
{
 Log_DBG("%s>%s",title,Log_buf2str(d, size ));
}
