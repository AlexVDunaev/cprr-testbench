-- лучше сразу загрузить файлы испытаний, что бы проверить корректность синтаксиса на старте
dofile("./tb.script/tests/demo.lua")
dofile("./tb.script/tests/power.lua")
dofile("./tb.script/tests/radarTest.lua")
dofile("./tb.script/tests/genStep.lua")
--

runTestModule = nil

-- прием команд от GUI. 

-- Специальные команды: EXIT
function onEventCommand(cmd, value)
  log("GUI CMD> "..cmd.." --> "..value)
  if (cmd == "CMD") and (value == "EXIT") then
    GUI.Log("EXIT")
    mcFunc("EXIT",1)  
    end

end
-- События выбора теста
function onRunTest(testName)
  runTestModule = testName
end

-- Функция - запускатель тестов
function runCurrentTest(runTestModule)
    if runTestModule then
      GUI.Log("[TEST RUN] --> [ " .. runTestModule.." ]")
      
      if (runTestModule == "DEMO") then
        TEST_MODULE_DEMO()
      end
      if (runTestModule == "POWER") then
        dofile("./tb.script/tests/power.lua")
        TEST_MODULE_POWER()
      end
      if (runTestModule == "TEST2") then
        TEST_RADAR_TARGET()
      end
      
      if (runTestModule == "RFGEN") then
        dofile("./tb.script/tests/genStep.lua")
        TEST_MODULE_GENDOWN()
      end
     
      
      if (runTestModule == "CONNECT_TEST") then
        TEST_RADAR_TARGET_xN()
      end
      if (runTestModule == "RADARALGTEST") then
        -- обновим файл проверки (можно править между пусками проверки...)
        dofile("./tb.script/tests/radarTest.lua")
        switch_to_raw_mode()
        --while 1 == 1 do
         local result = TEST_RADAR_ALG_CMP()
         REPORT.new(RadarSN, "Тест алгоритма радара")
         if ((result)and(result > 0)) then
          REPORT.add("РЕЗУЛЬТАТ: ОК")
         end
         REPORT.done()
         --end
        --mcFunc("EXIT",1)  
      end
      if (runTestModule == "TESTDIALOG") then
        GUI.Log("WAIT...")
        GUI.Dialog("design/rfgenErr.htm", "wait") --, "TEXT кода ошибки...!!")
        GUI.Log("USER READY")
      end
      -----------------------------------
      -- сюда дополнять запуск циклограмм
      -- .....
      -----------------------------------
      end
end

---------------------------------------
-- Точка входа в программу испытаний --
---------------------------------------
function runTest()

  local waitCount = 0
  log("TESTING> ------- START --------")
  -- Установим обработчик на прием команд от GUI
  GUI.OnEvent = onEventCommand
  GUI.OnRunTest = onRunTest
  -- Ожидание команды...
  while 1 == 1 do
    if (runTestModule) then
      ------------------------------
      runCurrentTest(runTestModule)
      ------------------------------
      GUI.Done(1)
      runTestModule = nil
      end
    pause(100*1000) -- засыпаем на 0.1c
    -- индикация ожидания 
    waitCount = waitCount + 1
    if waitCount > 50 then
      waitCount = 0
      log("Wait command...")
      -- GUI.Log("Wait command...")
    end
  end
  log("TESTING> ------- DONE  --------")

end




