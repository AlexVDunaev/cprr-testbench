
-- Параметры проверки
START_FREQ = 24.05456*1E9  -- начальная частота
STOP_FREQ  = 24.05368*1E9  -- конечная частота
STEP_FREQ  =     0.21*1E6  -- шаг частоты

-- переменные модуля АЧХ
algFR = {workDirectory = nil,  -- директория алгоритма
         frameCount    = 0,    -- счетчик полученных кадров обработчиком стриминга 
         mmapDrv       = nil,  -- драйвер MMAP через, который обмениваемся данными с модулем алгоритма
         alg           = nil,  -- таблица методов Алгоритма CPRR
         isBreak       = nil,  -- признак прерывания теста пользователем / по ошибке оборудования
         errorText     = nil   -- сообщение об ошибке оператору
         }


-- Определим обработчик ошибки (что-то с ИП)
local function GenFatalError(code)
  log(string.format("GenFatalError> code : %s", code))
  -- аварийное приведение в исх. состояние
  algFR.errorText = "Генератор N5183B не отвечает!"
  GUI.Log(algFR.errorText, "Red")
  algFR.isBreak = 1
  end
-- обработчика прерывания теста пользователем
local function onBreakFRGen()
  -- аварийное приведение в исх. состояние
  algFR.errorText = "Тест прерван"
  GUI.Log(algFR.errorText, "Red")
  algFR.isBreak = 1
  end


-- функция копирования файлов
local function CopyFile(old_path, new_path)
  local old_file = io.open(old_path)
  local new_file = io.open(new_path, "wb")
  if not old_file or not new_file then
    return false
  end
  while true do
    local block = old_file:read(2^13)
    if not block then break end
    new_file:write(block)
  end
  old_file:close()
  new_file:close()
  return true
end

local function algLoadDebugFrames(bagName, N)
  local head
  local u
  local i
  DATASET = {}
    for i = 0, N - 1 do
      u = algFR.alg.getFrame(bagName, i);
      DATASET[i] = u
    end
end


local function algGetDebugFrame(ptrData, frameIndex)
  local head
  local body
  if (DATASET) then
    if (DATASET[frameIndex]) then
      -- подменяю данные из датасета (только для отладки, когда нет стенда, только радар)
      head = algFR.alg.getField(ptrData, 0, 52+4*720*16*2).mem
      body = algFR.alg.getField(DATASET[frameIndex], 0, 64*720*16*2).mem
      ptrData = head..body
     end
   end
  return ptrData
end


-- Обработчик события стриминга
function userStreamEventGenNext(time, GNumber, t1, t2, t3, t4, t5, t6, ptrData, sizeOfData)
  local f = 0
  
  -- Кол-во кадров, полученных в стрименге
  algFR.frameCount = algFR.frameCount + 1
  
  if (RF_GEN) then
    f = RF_GEN.State.Freq
    RF_GEN.SetFreq(f - STEP_FREQ)
   end
  log(string.format("CPRRLineEvent1>[RX]: GNumber: %4d [%d %d %d %d %d %d] : F:%.8fGHz", GNumber, t1, t2, t3, t4, t5, t6, f/1E9)) 
  
  -- пауза для исключения переходных процессов генератора
  pause(100*1000) -- 0.1c
  
  -- Инициализация драйвера mmap (Однократно)
  if ((algFR.mmapDrv == nil)and(algFR.workDirectory)) then
    algFR.mmapDrv = algFR.alg.mmapInit(algFR.workDirectory.."/rfFrameBuff.bin", sizeOfData) -- init mmap file
  end
  
  -- отладочный код, для работы без стенда (но с радаром), подмена данных на данные из файла
  -- ptrData = algGetDebugFrame(ptrData, algFR.frameCount - 1)

  -- Передача кадра данных в mmap и сигнал алгоритму, что нужно обработать данные кадра
  if (algFR.mmapDrv) then
    algFR.alg.mmapWrite(algFR.mmapDrv, ptrData)
    GUI.RunAlg("RFGen", "CALC", "wait") 
  end
  
  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algFR.frameCount / FREQ_POINT_NUM))
  
  -- Проверка на прерывание теста
  if (algFR.isBreak) then
    return "BREAK" -- прекращаем стриминг
    end

end


local function buildReport(result)
  local i
  
  REPORT.new(RadarSN, "Тест АЧХ приемных трактов радара")
  REPORT.add("Графики результатов измерений АЧХ", "h3") -- формат заголовка
  REPORT.add("Начальная частота: "..string.format("%.5fGHz", START_FREQ / 1E9))
  REPORT.add("Конечная частота: "..string.format("%.5fGHz", STOP_FREQ / 1E9))
  REPORT.add("Шаг частоты: "..string.format("%.1fKHz, Точек контроля : %d", STEP_FREQ / 1E3, FREQ_POINT_NUM))

  -- копирование файлов графиков из директории алгоритма в директорию отчета
  for i = 1, 2 do
    if (CopyFile(string.format("%s/rfgenResultAF%d.png", algFR.workDirectory, i), 
             string.format("%s/rfgenResultAF%d.png", "./reports", i))) then
    
    REPORT.add(nil, nil, string.format([[<img src="rfgenResultAF%d.png" alt="RF_GEN RESULT%d">]], i, i))
    end
  end

  if (CopyFile(algFR.workDirectory .. "/rfgenResult.png", "./reports/rfgenResult.png")) then
    REPORT.add(nil, nil, [[<img src="rfgenResult.png" alt="RF GEN ALL MAX">]])
  end

  -- копируем картинку с FFT для каждого из 16 каналов
  for i = 0, 15 do
    CopyFile(string.format("%s/rfgenResultEx_%d.png", algFR.workDirectory, i), 
             string.format("%s/rfgenResultEx_%d.png", "./reports", i))
    REPORT.add(nil, nil, string.format([[<img src="rfgenResultEx_%d.png" alt="RF GEN CH%d">]], i, i))
    end
  REPORT.done()

end


-- циклограмма пошагового изменения частоты и записи датасета
function TEST_MODULE_GENDOWN()
  local result    -- итоговый результат проверки (получаем от модуля алгоритма)
  local i
  if (RF_GEN == nil) then
    GUI.Log("Ошибка генератора! Его нет в системе")
    log("Ошибка генератора! Его нет в системе")
    return 0
  end
  
  GUI.Clear()
  GUI.Progress(0)
  GUI.Log("Старт проверки АЧХ приемников...")
  if (GUI.Dialog("design/rfgen.htm", "wait") ~= "OK") then
    GUI.Log("Проверка отменена!")
    return 0
  end
  GUI.Log("Проверка началась")
  -- установка обработчика прерывания теста пользователем
  GUI.OnBreak = onBreakFRGen
  -- инициализация данных теста
  algFR.alg        = Algorithm_Init()
  algFR.isBreak    = nil
  algFR.mmapDrv    = nil
  algFR.errorText  = "[Не удалось получить данные с радара]" -- ошибка по умолчанию
  algFR.frameCount = 0
  -- инициализация алгоритма (очистка буферов данных) Возврат: путь к файлам
  algFR.workDirectory = GUI.RunAlg("RFGen", "START", "wait")
  --------------
  FREQ_POINT_NUM = math.floor((START_FREQ - STOP_FREQ) / STEP_FREQ) + 1 -- кол-во точек контроля
  
  -- для отладки. читаю данные из файла, а не из радара
  -- algLoadDebugFrames("./new.001.005.t25.bag", FREQ_POINT_NUM)

  if (RF_GEN) then
    -- установка обработчика ошибок генератора 
    RF_GEN.OnError = GenFatalError
    RF_GEN.SetFreq(START_FREQ)
    RF_GEN.SetPower(-10.0)    
    RF_GEN.Output(0)
  end
  --
  
  local nTry -- кол-во попыток получить данные
  -- дальнейшая работа по изменению частоты идет в userStreamEventGenNext
  RADAR.StreamEvent = userStreamEventGenNext;
  for nTry = 1,3 do
    -- Установка начальной частоты при начале стрима
    if (RF_GEN) then
      RF_GEN.SetFreq(START_FREQ)
    end
    -- Начнем сбор данных ...
    RADAR.SaveStream("./reports/rf_gen.bag", FREQ_POINT_NUM, 5) -- 5 : Режим стриминга TX:OFF, ЛЧМ:OFF
    -- Выход из режима стриминга. 
    -- Если данных не было, повторим...
    if (algFR.frameCount >= FREQ_POINT_NUM)or(algFR.isBreak) then
        break -- все норм. (или просто прервали)
    end
  end
  
  RADAR.StreamEvent = nil
  GUI.OnBreak = nil

  if (RF_GEN) then
    RF_GEN.SetPower(-39.0)    
    RF_GEN.Output(0)
    RF_GEN.OnError = nil
  end
  
  if (algFR.mmapDrv) then
    -- Закроем драйвер MMAP
    algFR.alg.mmapClose(algFR.mmapDrv)
    end
  
  if (algFR.frameCount < FREQ_POINT_NUM) then
    -- Что-то пошло не так... не то количество кадров...
    GUI.Dialog("design/rfgenErr.htm", "wait", algFR.errorText)
    GUI.Log("Ошибка")
    return 0
  end
  
  -- Завершим алгоритм. Построение графика АЧХ, получим итоговый результат
  -- финальный расчет занимает 2 .. 5 с
  result = GUI.RunAlg("RFGen", "DONE", "wait")
  
  -- Формирование отчета:
  buildReport(result)
  
  GUI.Log("Проверка завершилась, ждем реакции пользователя...")
  GUI.Dialog("design/rfgenDone.htm", "wait")
  GUI.Log("Конец")
  
end