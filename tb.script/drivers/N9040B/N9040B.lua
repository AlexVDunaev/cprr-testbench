--[[
FREQ:CENT? ->2.4000000000E+10
FREQ:CENT 24.1GHz
FREQ:SPAN 255MHz
FREQ:SPAN 250000000;:FREQ:SPAN? -> 2.504882813E+08
*IDN? ->Keysight Technologies,N9040B,US57212285,A.26.10
:CALC:MARK1:MODE POS [появляется маркер 1]
CALC:MARK:X? ->1.04000000000E+08
CALC:MARK1:X?
CALC:MARK:X 460000000 [Установка маркера на нужную позицию]
CALC:MARK1:X? ->4.60000000000E+08
CALC:MARK:X 24000000000;:CALC:MARK1:X? ->2.4000000000000E+10
CALC:MARK:Y? ->-6.3940E+01
CALC:MARK1:X 24000000000;:CALC:MARK1:X?;:CALC:MARK1:Y? -> 2.4000000000000E+10;-6.3778E+01
FREQ:STAR 2.39E+10
FREQ:STOP 2.405E+10 [меняет SPAN]
 
[Управление инструментами. RTSA - RealTime SA]
:INST? ->SA 
:INST? ->RTSA
:INST SA [переход на SA ~3 сек]
:INST RTSA;:INST? ->RTSA[Если режим не нужно переключать возврат сразу]
:INST SA;:INST? ->SA

CALC:MARK1:X 24000000000.000000;:CALC:MARK1:X?
:CALC:MARK2:X 24010000000.000000;:CALC:MARK2:X? -> 9.91E+37 [нет маркера]
:CALC:MARK2:MODE POS;:CALC:MARK2:X 24010000000.000000;:CALC:MARK2:X?
:MMEM:STOR:SCR "myScreen.png" ["My Documents\RTSA\screen\myScreen.png"]
DISP:WIND:TEXT:DATA "TEST RADAR" [??? не работает!]


:MMEM:STOR:TRAC:DATA TRACE1,"myTrace23.csv" [Сохраняет файл с тек. измерениями + заголовок]

]]

N9040B_TCP_IP = "192.168.1.40" -- default IP
N9040B_TCP_PORT = 5025        -- default port
N9040B_TIMEOUT = 2000
N9040B_TRY_NUM = 3


function N9040B_Float2Str(v)
  local strValue;
  strValue = string.format("%.8E", v)
  if (v >= 0) then
    strValue = "+" .. strValue
    end
  return strValue
end

function N9040B_Emul()
  local cmd;
  local value;
  cmd = N9040B.State.LastCmd
  value = N9040B.State.LastValue
  if (cmd == "FREQ") then
    N9040B.State.Freq = value
    return "OK", N9040B_Float2Str(value).."\n"
    end
  if (cmd == "SPAN") then
     if (value == nil) then
      value = 255E6 -- 255MHz
      end
    N9040B.State.Span = value
    return "OK", N9040B_Float2Str(value).."\n"
  end
  if (cmd == "MARK") then
    N9040B.State.Marker = value
    N9040B.State.MarkerY = N9040B.State.MarkerY + 1
    return "OK", N9040B_Float2Str(value)..";"..N9040B_Float2Str(N9040B.State.MarkerY).."\n"
  end
  if (cmd == "MODE") then
    N9040B.State.Mode = value
    return "OK", value.."\n"
  end
  
  return "ERR", ""
end


function N9040B_reqCmd(minLength, regExpFilter, limitTimeOut)
  if (isEmul == 1) then
    return N9040B_Emul()
    end
  buffer = ""
  stepTimeout = 50
  while true do
    -- читаем TCP
    rxResult, bufferRx = mcLinkRx(N9040B.Line, minLength, stepTimeout)
    
    if (rxResult == "OK") then
     buffer = buffer .. bufferRx
     if (string.match(buffer, regExpFilter)) then
       -- есть подходящий буфер
       -- print("N9040B_reqCmd : [OK] -> ", buffer)
       return "OK", buffer
     end
     -- Нужного ответа пока нет, продолжаем запрашивать данные
    if (limitTimeOut <= stepTimeout) then
      -- общий таймаут вышел !
      print("TIMEOVER")
      return "ERR" , ""
    else
    -- print("NEXT")
    end
    limitTimeOut = limitTimeOut - stepTimeout
    -- print("limitTimeOut : " .. limitTimeOut)
     else
      print("LINE ERROR!")
      return "ERR" , ""
    end
    end
  end


-- Для команд установки, в которых есть переменная состовляющая, сохраняем не полную команду, а ID группы команд 
function N9040B_SendCmd(cmd, cmdId, value) 
 N9040B.State.LastCmd = cmd
 N9040B.State.LastValue = value
 if (cmdId) then
   N9040B.State.LastCmd = cmdId
 end
 mcLinkTx (N9040B.Line, cmd .. "\n")
 -- print("SCPI>"..cmd)
end

-- Универсальная функция посылки SCPI команды [3 повтора] и ожидание ответа нужного формата. Генерация ошибки если не получен ответ
function N9040B_Request(strCmd, idCmd, valueCmd, resultNum, minLength, regExpFilter, limitTimeOut, regExpExtract, labelString) 
  local i;
  local v1;
  local v2;
-- три попытки запросить прибор
 for i = 1, N9040B_TRY_NUM do 
   
   N9040B_SendCmd(strCmd, idCmd, valueCmd)
   
   result, buffer = N9040B_reqCmd(minLength, regExpFilter, limitTimeOut)
   
   if (result == "OK") then
    if (resultNum == 1) then
      v1 = string.match(buffer, regExpExtract)
      if (v1 ~= nil) then
       return v1
      end
    end
    if (resultNum == 2) then
      v1, v2 = string.match(buffer, regExpExtract)
      if (v1 ~= nil)and(v2 ~= nil) then
        return v1, v2
      end
    end
   end
  print(labelString .. " - ERROR!")
  end
  print(labelString .. " - FATAL ERROR!")
  if (N9040B.OnError) then
    N9040B.OnError(string.format("%s(%s)", labelString, valueCmd))
    end
  return nil
end

--------------------------------------------------
-- Установка [+проверка] Центральной частоты. "FREQ:CENT 24.1GHz;:FREQ:CENT?
--------------------------------------------------
function N9040B_SetFreq(v) 
 local cmdStr;
 local f;
 cmdStr = string.format("FREQ:CENT %f;:FREQ:CENT?", v)
 f = N9040B_Request(
         cmdStr,  -- команда
         "FREQ",  -- идентификатор команды (для эмулятора)
         v,       -- значение параметра (для эмулятора)
         1,       -- кол-во возвращаемых параметров
         #("2.4000000E+10\n"),        -- минимальная длина ответа (одно число %E формата)
         "%d+%.%d+E[-+]%d%d%d*\n$",   -- regExp, описывающее SCPI ответ
         N9040B_TIMEOUT,              -- Предельное время ожидания ответа
         '(%d+%.%d+E[-+]%d%d%d*)\n$', -- regExp, с захватами, для извлечения параметров ответа
         "SetFreq"  -- Строка-идентификатор, используемая при выдачи сообщения от ошибке
       )
  if (f) then
    N9040B.State.Freq = tonumber(f)
    return N9040B.State.Freq
    end
end


--------------------------------------------------
-- Установка [+проверка] SPAN. FREQ:SPAN 250000000;:FREQ:SPAN? -> 2.504882813E+08
-- v == nil : SPAN:FULL
--------------------------------------------------
function N9040B_SetSpan(v) 
 local cmdStr;
 local f;
 if ((v == 0)or(v == nil)) then
   cmdStr = "FREQ:SPAN:FULL;:FREQ:SPAN?"
   else
   cmdStr = string.format("FREQ:SPAN %f;:FREQ:SPAN?", v)
   end
 
  f = N9040B_Request(
         cmdStr,  -- команда
         "SPAN",  -- идентификатор команды (для эмулятора)
         v,       -- значение параметра (для эмулятора)
         1,       -- кол-во возвращаемых параметров
         #("2.4000000E+10\n"),        -- минимальная длина ответа (одно число %E формата)
         "%d+%.%d+E[-+]%d%d%d*\n$",   -- regExp, описывающее SCPI ответ
         N9040B_TIMEOUT,              -- Предельное время ожидания ответа
         '(%d+%.%d+E[-+]%d%d%d*)\n$', -- regExp, с захватами, для извлечения параметров ответа
         "SetSpan"  -- Строка-идентификатор, используемая при выдачи сообщения от ошибке
       )
  if (f) then
    N9040B.State.Span = tonumber(f)
    return N9040B.State.Span
    end
end


--------------------------------------------------
-- Установка [+проверка + чтение dB] маркера. CALC:MARK:X 24000000000;:CALC:MARK1:X?;:CALC:MARK1:Y?
--------------------------------------------------
function N9040B_SetMarker(v) 
 local cmdStr;
 local f;
 local dB;
 
 cmdStr = string.format("CALC:MARK1:X %f;:CALC:MARK1:X?;:CALC:MARK1:Y?", v)
 
 if (N9040B_isMarkerSetMode == nil) then
   -- Нужно "создать" маркер
   cmdStr = ":CALC:MARK1:MODE POS;:"..cmdStr;
   N9040B_isMarkerSetMode = 1
 end
  
  f, dB = N9040B_Request(
         cmdStr,  -- команда
         "MARK",  -- идентификатор команды (для эмулятора)
         v,       -- значение параметра (для эмулятора)
         2,       -- кол-во возвращаемых параметров
         #("2.4000000E+10;-6.3778E+01\n"),  -- минимальная длина ответа (одно число %E формата)
         "%d+%.%d+E[-+]%d%d%d*;[-+]*%d+%.%d+E[-+]%d%d%d*\n$",   -- regExp, описывающее SCPI ответ
         N9040B_TIMEOUT,              -- Предельное время ожидания ответа
         '(%d+%.%d+E[-+]%d%d%d*);([-+]*%d+%.%d+E[-+]%d%d%d*)\n$', -- regExp, с захватами, для извлечения параметров ответа
         "SetMarker"  -- Строка-идентификатор, используемая при выдачи сообщения от ошибке
       )
 
    if (f ~= nil)and(dB ~= nil) then
      f = tonumber(f)
      dB = tonumber(dB)
      N9040B.State.Marker = f
      N9040B.State.MarkerY = dB
      return f, dB
      end
end



--------------------------------------------------
-- Установка [+проверка] Режима работы. :INST RTSA;:INST?
--------------------------------------------------
function N9040B_SetMode(v) 
 local cmdStr;
 local mode;
 
 cmdStr = string.format(":INST %s;:INST?", v)
 
  mode = N9040B_Request(
         cmdStr,  -- команда
         "MODE",  -- идентификатор команды (для эмулятора)
         v,       -- значение параметра (для эмулятора)
         1,       -- кол-во возвращаемых параметров
         #("SA\n"),  -- минимальная длина ответа (одно число %E формата)
         "%a+\n$",   -- regExp, описывающее SCPI ответ
         10*1000,    -- Предельное время ожидания ответа (Для смены режима до 10 с)
         '(%a+)\n$', -- regExp, с захватами, для извлечения параметров ответа
         "SetMode"   -- Строка-идентификатор, используемая при выдачи сообщения от ошибке
       )

  N9040B.State.Mode = mode
  return mode
end


--------------------------------------------------
-- Команда сохранить скриншот. 
--------------------------------------------------
function N9040B_PrintScreen(v) 
 N9040B_SendCmd(string.format(':MMEM:STOR:SCR "%s"', v))
end
--------------------------------------------------
-- Команда сохранить данные. 
--------------------------------------------------
function N9040B_SaveData(v) 
 N9040B_SendCmd(string.format(':MMEM:STOR:TRAC:DATA TRACE1,"%s"', v))
end




function N9040B_Init(IP, PORT)
  log(string.format("N9040BInit( [IP: %s:%d] )", IP, PORT)) 
  if (DriverLineId) then
    DriverLineId = DriverLineId + 1
  else
    DriverLineId = 1
  end

  N9040B = {}
  N9040B.Line    = DriverLineId
  N9040B.OnError = nil
  N9040B.SetFreq = N9040B_SetFreq
  N9040B.SetMarker = N9040B_SetMarker
  N9040B.SetMode = N9040B_SetMode
  N9040B.SetSpan = N9040B_SetSpan
  N9040B.PrintScreen = N9040B_PrintScreen
  N9040B.SaveData = N9040B_SaveData
  N9040B.SendSCPI = N9040B_SendCmd
  N9040B.State = {LastCmd = "", LastValue = 0, Freq = 0, Mode = "", Span = 0, Marker = 0, MarkerY = -100}

  if (IP) then -- nil --> use default
   N9040B_TCP_IP = IP
  end
  if (PORT) then -- nil --> use default
   N9040B_TCP_PORT = PORT
  end

  addObject("LINE",   "LINETXSP",  N9040B.Line, "[nUse]", N9040BLineProcTX)
  return N9040B
end


function N9040BLineProcTX(idEvent, buffer, LineName)
if (idEvent == "INIT") then
 mcLinkOpen (N9040B.Line, "TCP", N9040B_TCP_IP, tostring(N9040B_TCP_PORT), "DUMMY")
 end
return "OK", ""
end


