--[[
Список команд ИП:
MEASure:CURRent?;:MEASure:VOLT?$0A --> +0.00000000E+000;+7.00000000E-002{0A}
CURRent 1;:CURRent?$0A --> +1.00000000E+000{0A}
VOLTage 10;:VOLTage?{0A} -->+1.00000000E+001{0A}
OUTP ON{0A} --> [пусто!]
OUTP ON;:OUTPut:STATe?$0A --> 1{0A}
OUTP OFF;:OUTPut:STATe?$0A --> 0{0A}
*IDN?$0A --> Agilent Technologies,N5770A,US20B1798P,C.00.10.328,REV:F{0A}
SYSTem:VERSion?$0A --> 1995.0{0A}
SYSTem:ERRor?$0A --> +0,"No error"{0A}
CURRent:PROTection:STATe?$0A --> 0{0A}
VOLTage:LIMit:LOW?$0A -->+0.00000000E+000{0A}
VOLTage:PROTection:LEVel?$0A -->+1.65000000E+002{0A}
VOLTage:PROTection:LEVel 164 $0A -->[пусто!]
VOLTage:PROTection:LEVel 165;:VOLTage:PROTection:LEVel?$0A -->+1.65000000E+002{0A}
]]

N5770_TCP_IP = "192.168.1.222" -- default IP
N5770_TCP_PORT = 5025        -- default port
N5770_TIMEOUT = 2000
N5770_TRY_NUM = 3

N5770_LastCmd = ""

N5770_Emul_V = 0.0
N5770_Emul_I = 0.0

function N5770_Float2Str(v)
  local strValue;
  strValue = string.format("%.8E", v)
  if (v >= 0) then
    strValue = "+" .. strValue
    end
  return strValue
end

function N5770_Emul(cmd)
  if (cmd == "MEASure:CURRent?;:MEASure:VOLT?") then
    N5770_Emul_V = N5770_Emul_V + 1.2
    N5770_Emul_I = N5770_Emul_I + 0.15
    return "OK", N5770_Float2Str(N5770_Emul_I)..";"..N5770_Float2Str(N5770_Emul_V).."\n"
    end
  -- группа команд установки напряжения
  if (cmd == "VOLTage") then
    return "OK", N5770_Float2Str(N5770_Emul_V).."\n"
    end
  -- группа команд установки тока
  if (cmd == "CURRent") then
    return "OK", N5770_Float2Str(N5770_Emul_I).."\n"
  end
  if (cmd == "OUTP ON;:OUTPut:STATe?") then
    return "OK", "1\n"
  end
  if (cmd == "OUTP OFF;:OUTPut:STATe?") then
    return "OK", "0\n"
  end
  return "ERR", ""
end


function N5770_reqCmd(minLength, regExpFilter, limitTimeOut)
  if (isEmul == 1) then
    return N5770_Emul(N5770_LastCmd)
    end
  local buffer = ""
  local stepTimeout = 50
  local rxResult
  local bufferRx
  while true do
    -- читаем TCP
    rxResult, bufferRx = mcLinkRx(N5770.Line, minLength, stepTimeout)
    
    if (rxResult == "OK") then
     buffer = buffer .. bufferRx
     if (string.match(buffer, regExpFilter)) then
       -- есть подходящий буфер
       return "OK", buffer
     end
     -- Нужного ответа пока нет, продолжаем запрашивать данные
    if (limitTimeOut <= stepTimeout) then
      -- общий таймаут вышел !
      return "ERR" , ""
    else
    -- print("NEXT")
    end
    limitTimeOut = limitTimeOut - stepTimeout
    else
      print("LINE ERROR!")
      return "ERR" , ""
    end
    end
  end


-- Для команд установки, в которых есть переменная состовляющая, сохраняем не полную команду, а ID группы команд 
function N5770_SendCmd(cmd, cmdId) 
 N5770_LastCmd = cmd
 if (cmdId) then
   N5770_LastCmd = cmdId
   end
 mcLinkTx (N5770.Line, cmd .. "\n")
end

--------------------------------------------------
-- Установка [+проверка] напряжения. VOLTage 10;:VOLTage?
--------------------------------------------------
function N5770_SetVoltage(v) 
 local cmdStr;
 local i
 cmdStr = string.format("VOLTage %f;:VOLTage?", v)
-- три попытки запросить прибор
 for i = 1, N5770_TRY_NUM do 
   
   N5770_SendCmd(cmdStr, "VOLTage")
   
   result, buffer = N5770_reqCmd(#("+0.00000000E+00\n"), "[-+]%d+%.%d+E[-+]%d%d%d*\n$", N5770_TIMEOUT)
   
   if (result == "OK") then
    v1 = string.match(buffer, '([-+]%d+%.%d+E[-+]%d%d%d*)\n$')
    if (v1 ~= nil) then
      v1 = tonumber(v1)
      return v1
      end
    end
  print("SetVoltage() - ERROR!")
  end
  print("SetVoltage() - FATAL ERROR!")
  if (N5770.OnError) then
    N5770.OnError("SetVoltage")
    end
  return nil
end

--------------------------------------------------
-- Установка [+проверка] тока. CURRent 1;:CURRent?
--------------------------------------------------
function N5770_SetCurrent(v) 
 local cmdStr;
 local i
 
 cmdStr = string.format("CURRent %f;:CURRent?", v)
-- три попытки запросить прибор
 for i = 1, N5770_TRY_NUM do 
   
   N5770_SendCmd(cmdStr, "CURRent")
   
   result, buffer = N5770_reqCmd(#("+0.00000000E+00\n"), "[-+]%d+%.%d+E[-+]%d%d%d*\n$", N5770_TIMEOUT)
   if (result == "OK") then
    v1 = string.match(buffer, '([-+]%d+%.%d+E[-+]%d%d%d*)\n$')
    if (v1 ~= nil) then
      v1 = tonumber(v1)
      return v1
      end
    end
  print("SetCurrent() - ERROR!")
  end
  print("SetCurrent() - FATAL ERROR!")
  if (N5770.OnError) then
    N5770.OnError("SetCurrent")
    end
  return nil
end

--------------------------------------------------
-- Установка [+проверка] выхода. OUTP ON;:OUTPut:STATe?
--------------------------------------------------
function N5770_SetOutput(v) 
 local cmdStr;
 local i
 
 if (v > 0) then 
   cmdStr = "OUTP ON;:OUTPut:STATe?"
 else
   cmdStr = "OUTP OFF;:OUTPut:STATe?"
 end
 
 -- три попытки запросить прибор
 for i = 1, N5770_TRY_NUM do 
   
   N5770_SendCmd(cmdStr)
   
   result, buffer = N5770_reqCmd(#("0\n"), "%d\n$", N5770_TIMEOUT)
   if (result == "OK") then
    v1 = string.match(buffer, '(%d)\n$')
    if (v1 ~= nil) then
      v1 = tonumber(v1)
      return v1
      end
    end
  print("SetOutput() - ERROR!")
  end
  print("SetOutput() - FATAL ERROR!")
  if (N5770.OnError) then
    N5770.OnError("SetOutput")
    end
  return nil
  
end

function N5770_GetMeasure( ) 
 local i
 local result
 local buffer
 local v1
 local v2
 -- три попытки запросить прибор
 for i = 1, N5770_TRY_NUM do 
   
   N5770_SendCmd("MEASure:CURRent?;:MEASure:VOLT?")
   -- длина ожидаемого ответа, RegExp для фильтра, время ожидания ответа 
   -- формат числа может быть 8.35000000E+000 или 8.35000000E+00
   result, buffer = N5770_reqCmd(#("+0.00000000E+00;+8.35000000E+00\n"), "[-+]%d+%.%d+E[-+]%d%d%d*;[-+]%d+%.%d+E[-+]%d%d%d*\n$", N5770_TIMEOUT)
   if (result == "OK") then
    v1, v2 = string.match(buffer, '([-+]%d+%.%d+E[-+]%d%d%d*);([-+]%d+%.%d+E[-+]%d%d%d*)\n$')
    if (v1 ~= nil)and(v2 ~= nil) then
      v1 = tonumber(v1)
      v2 = tonumber(v2)
      return v1, v2
      end
    end
  print("GetMeasure() - ERROR!")
  end
  print("GetMeasure() - FATAL ERROR!")
  if (N5770.OnError) then
    N5770.OnError("GetMeasure")
    end
  return nil, nil
end

--------------------------------------------------
-- Запрос ID 
--------------------------------------------------
function N5770_GetIDN(v) 
 local i
 local result
 local buffer
 local v1
 local cmdStr;
 
 if (v > 0) then 
   cmdStr = "OUTP ON;:OUTPut:STATe?"
 else
   cmdStr = "OUTP OFF;:OUTPut:STATe?"
 end
 
 -- три попытки запросить прибор
 for i = 1, N5770_TRY_NUM do 
   
   N5770_SendCmd(cmdStr)
   
   result, buffer = N5770_reqCmd(#("0\n"), "%d\n$", N5770_TIMEOUT)
   if (result == "OK") then
    v1 = string.match(buffer, '(%d)\n$')
    if (v1 ~= nil) then
      v1 = tonumber(v1)
      return v1
      end
    end
  print("SetOutput() - ERROR!")
  end
  print("SetOutput() - FATAL ERROR!")
  if (N5770.OnError) then
    N5770.OnError("IDN")
    end
  return nil
  
end


function N5770_ReConnect() 
  return mcLinkCtrl(N5770.Line, "OPEN", N5770_TCP_IP, tostring(N5770_TCP_PORT))
end

function N5770_Init(IP, PORT)
  log(string.format("N5770Init( [IP: %s:%d] )", IP, PORT)) 

if (DriverLineId) then
    DriverLineId = DriverLineId + 1
  else
    DriverLineId = 1
  end
  
  N5770 = {}
  N5770.Line    = DriverLineId
  N5770.OnError = nil
  N5770.SetVoltage = N5770_SetVoltage
  N5770.SetCurrent = N5770_SetCurrent
  N5770.SetOutput  = N5770_SetOutput
  N5770.GetMeasure = N5770_GetMeasure
  N5770.CMD        = N5770_SendCmd
  N5770.ReConnect  = N5770_ReConnect

  if (IP) then -- nil --> use default
   N5770_TCP_IP = IP
  end
  if (PORT) then -- nil --> use default
   N5770_TCP_PORT = PORT
  end

  addObject("LINE",   "LINETX",  N5770.Line, "[nUse]", LineProcTX)
  return N5770
end


function LineProcTX(idEvent, buffer, LineName)
if (idEvent == "INIT") then
 if isEmul == 1 then
    mcLinkOpen (N5770.Line, "NULL", N5770_TCP_IP, tostring(N5770_TCP_PORT), "DUMMY")
   else
    mcLinkOpen (N5770.Line, "TCP", N5770_TCP_IP, tostring(N5770_TCP_PORT), "DUMMY")
   end
 end
return "OK", ""
end


