--REQID = 0
--WAITVALUE = "N"
--REQSG = ""

function SCPIInit()
  addObject("PROTOCOL.MANAGER", "SCPI", "FRAMING=RAW; POLLING=INTERNAL;", SCPIManagerProc)
end

Errors = 0
-- Формат packet на прием: [ИМЯ_СИГНАЛА при запросе] + [Тип] + [полученные данные]
-- Формат packet на передачу: командная строка для отправки 
function SCPIManagerProc(idEvent, signalName, signalValue, packet, LineID)
--print("SCPIManagerProc: ", idEvent, signalName, packet)
local sg
local vtype
local p
local p0
local targetName = ""

if (idEvent == "RX") then
--  log(string.format("SCPIManagerProc(RX) LineID = %d", LineID))
--  hexlog("ProtocolRX>", packet)
  if (string.len(packet) < 20) then
   Errors = Errors + 1
   log(string.format("!!!!!!!!SCPIManagerProc(ERRRRRRRR) LineID = %d (ERR = %d)", LineID, Errors))
--   return "OK", ""
   end
  
--  log(string.format("SCPIManagerProc(RX)> %s", packet))
        sg, vtype, targetName, p = string.unpack("zzzz", packet)
 if (string.find(p, "\n")) then
  p0 = p
  p = string.sub(p, 1, string.find(p, "\n"))
--  log(string.format("SCPI> sub %s -> %s", p0, p))
  end
--  log(string.format("sg = %s, t = %s, tg = %s, p = %s", sg, vtype, targetName, p))
        if (vtype == "NUM") then
	value = tonumber(p)
	if value == nil then
	  log(string.format("Signal %s. Error in response message %s", sg, p))
	else
	 if (string.len(targetName) > 0) then
--	  log(string.format("target = %s", targetName))

	  set(targetName, value) -- Notify Client
	--     set(AgTargetName[AgGetIndex(sgName)], value) -- Notify Client
	  end
--        log(string.format("sg = %s <- %f", sg,  value))
	set(sg, value)
	end
        return "OK", ""
        end
        if (vtype == "STR") then
 		set(sg, p)
	        return "OK", ""
        end
  return "OK", "" 
  end
if (idEvent == "REQ") then
--  REQSG = signalName
--  REQID = 0 + mcGetParam(signalName, "REQID")
--  WAITVALUE = mcGetParam(signalName, "WAITVALUE")
  return "OK", packet
  end
return "ERROR", ""
end


function IP2HexInt(ip) -- '192.168.6.4' -> 0xC0A80604 
local result
local i0
local i1
local s
ip = "." .. ip .. "."
result = "0x"
for i=1, 4 do
 i0, i1 = string.find(ip, ".%d+.")
 s = string.sub(ip, i0 + 1, i1 - 1)
 result = result .. string.format("%02X", (s + 0))
 ip = string.sub(ip, i1)
 end
-- print(result)
return result
end
