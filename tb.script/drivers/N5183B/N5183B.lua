

N5183B_TCP_IP = "192.168.1.222" -- default IP
N5183B_TCP_PORT = 5025        -- default port

N5183B_LastCmd = ""
--[[

*IDN? ->Agilent Technologies, N5183B, MY59100736, B.01.86
FREQ 25GHz [нет ответа]
FREQ? ->+2.5000000000000E+10
FREQ 2.40E+10;:FREQ? ->+2.4000000000000E+10
POWER -39.0;:POWER? -> -3.90000000E+001
OUTPut 1;:OUTPut? -> 1
OUTPut 0;:OUTPut? -> 0
]]

N5183B_TCP_IP = "192.168.1.100" -- default IP
N5183B_TCP_PORT = 5025        -- default port
N5183B_TIMEOUT = 2000
N5183B_TRY_NUM = 3


function N5183B_Float2Str(v)
  local strValue;
  strValue = string.format("%.8E", v)
  if (v >= 0) then
    strValue = "+" .. strValue
    end
  return strValue
end

function N5183B_Emul()
  local cmd;
  local value;
  cmd = N5183B.State.LastCmd
  value = N5183B.State.LastValue
  if (cmd == "FREQ") then
    N5183B.State.Freq = value
    return "OK", N5183B_Float2Str(value).."\n"
    end
  if (cmd == "SPAN") then
     if (value == nil) then
      value = 255E6 -- 255MHz
      end
    N5183B.State.Span = value
    return "OK", N5183B_Float2Str(value).."\n"
  end
  if (cmd == "MARK") then
    N5183B.State.Marker = value
    N5183B.State.MarkerY = N5183B.State.MarkerY + 1
    return "OK", N5183B_Float2Str(value)..";"..N5183B_Float2Str(N5183B.State.MarkerY).."\n"
  end
  if (cmd == "MODE") then
    N5183B.State.Mode = value
    return "OK", value.."\n"
  end
  
  return "ERR", ""
end


function N5183B_reqCmd(minLength, regExpFilter, limitTimeOut)
  if (isEmul == 1) then
    return N5183B_Emul()
    end
  buffer = ""
  stepTimeout = 50
  -- print("reqCmd(minLength : " .. minLength .. ")")
  while true do
    -- читаем TCP
    rxResult, bufferRx = mcLinkRx(N5183B.Line, minLength, stepTimeout)
    
    if (rxResult == "OK") then
     buffer = buffer .. bufferRx
     if (string.match(buffer, regExpFilter)) then
       -- есть подходящий буфер
       -- print("reqCmd : [OK] -> ", buffer)
       return "OK", buffer
     end
     -- Нужного ответа пока нет, продолжаем запрашивать данные
    if (limitTimeOut <= stepTimeout) then
      -- общий таймаут вышел !
      print("TIMEOVER")
      return "ERR" , ""
    else
    -- print("NEXT")
    end
    limitTimeOut = limitTimeOut - stepTimeout
    -- print("limitTimeOut : " .. limitTimeOut)
     else
      print("LINE ERROR!")
      return "ERR" , ""
    end
    end
  end


-- Для команд установки, в которых есть переменная состовляющая, сохраняем не полную команду, а ID группы команд 
function N5183B_SendCmd(cmd, cmdId, value) 
 N5183B.State.LastCmd = cmd
 N5183B.State.LastValue = value
 if (cmdId) then
   N5183B.State.LastCmd = cmdId
 end
 mcLinkTx (N5183B.Line, cmd .. "\n")
end

-- Универсальная функция посылки SCPI команды [3 повтора] и ожидание ответа нужного формата. Генерация ошибки если не получен ответ
function N5183B_Request(strCmd, idCmd, valueCmd, resultNum, minLength, regExpFilter, limitTimeOut, regExpExtract, labelString) 
 local i;
 local v1;
 local v2;
 local result
 local buffer
 
-- три попытки запросить прибор
 for i = 1, N5183B_TRY_NUM do 
   
   N5183B_SendCmd(strCmd, idCmd, valueCmd)
   
   result, buffer = N5183B_reqCmd(minLength, regExpFilter, limitTimeOut)
   -- print(buffer)
   if (result == "OK") then
    if (resultNum == 1) then
      v1 = string.match(buffer, regExpExtract)
      if (v1 ~= nil) then
       return v1
      end
    end
    if (resultNum == 2) then
      v1, v2 = string.match(buffer, regExpExtract)
      if (v1 ~= nil)and(v2 ~= nil) then
        return v1, v2
      end
    end
   end
  print(labelString .. " - ERROR!")
  end
  print(labelString .. " - FATAL ERROR!")
  if (N5183B.OnError) then
    N5183B.OnError(string.format("%s(%s)", labelString, valueCmd))
    end
  return nil
end

--------------------------------------------------
-- Установка [+проверка] частоты. "FREQ 24.1GHz;:FREQ?
--------------------------------------------------
function N5183B_SetFreq(v) 
 local cmdStr;
 local f;
 cmdStr = string.format("FREQ %f;:FREQ?", v)
 f = N5183B_Request(
         cmdStr,  -- команда
         "FREQ",  -- идентификатор команды (для эмулятора)
         v,       -- значение параметра (для эмулятора)
         1,       -- кол-во возвращаемых параметров
         #("2.4000000E+10\n"),        -- минимальная длина ответа (одно число %E формата)
         "[-+]%d+%.%d+E[-+]%d%d%d*\n$",   -- regExp, описывающее SCPI ответ
         N5183B_TIMEOUT,              -- Предельное время ожидания ответа
         '([-+]%d+%.%d+E[-+]%d%d%d*)\n$', -- regExp, с захватами, для извлечения параметров ответа
         "SetFreq"  -- Строка-идентификатор, используемая при выдачи сообщения от ошибке
       )
  if (f) then
    N5183B.State.Freq = tonumber(f)
    return N5183B.State.Freq
    end
end

--------------------------------------------------
-- Установка [+проверка] мощности сигнала. "POWER -39.0;:POWER? --> -3.90000000E+001
--------------------------------------------------
function N5183B_SetPower(v) 
 local cmdStr;
 local f;
 cmdStr = string.format("POWER %f;:POWER?", v)
 f = N5183B_Request(
         cmdStr,  -- команда
         "POWER", -- идентификатор команды (для эмулятора)
         v,       -- значение параметра (для эмулятора)
         1,       -- кол-во возвращаемых параметров
         #("-3.900000E+001\n"),        -- минимальная длина ответа (одно число %E формата)
         "[-+]%d+%.%d+E[-+]%d%d%d*\n$",   -- regExp, описывающее SCPI ответ
         N5183B_TIMEOUT,              -- Предельное время ожидания ответа
         '([-+]%d+%.%d+E[-+]%d%d%d*)\n$', -- regExp, с захватами, для извлечения параметров ответа
         "SetPower"  -- Строка-идентификатор, используемая при выдачи сообщения от ошибке
       )
  if (f) then
    N5183B.State.Power = tonumber(f)
    return N5183B.State.Power
    end
end

--------------------------------------------------
-- Включение/отключение выхода генератора. OUTPut 1;:OUTPut? -> 1
--------------------------------------------------
function N5183B_SetOutput(v) 
 local cmdStr;
 local f;
 cmdStr = string.format("OUTPut %d;:OUTPut?", v)
 f = N5183B_Request(
         cmdStr,  -- команда
         "OUTPUT", -- идентификатор команды (для эмулятора)
         v,       -- значение параметра (для эмулятора)
         1,       -- кол-во возвращаемых параметров
         #("1\n"),        -- минимальная длина ответа (одно число %E формата)
         "%d\n$",   -- regExp, описывающее SCPI ответ
         N5183B_TIMEOUT,              -- Предельное время ожидания ответа
         '(%d)\n$', -- regExp, с захватами, для извлечения параметров ответа
         "SetOutput"  -- Строка-идентификатор, используемая при выдачи сообщения от ошибке
       )
  if (f) then
    N5183B.State.Out = tonumber(f)
    return N5183B.State.Out
    end
end


function N5183B_Init(IP, PORT)
  log(string.format("N5183BInit( [IP: %s:%d] )", IP, PORT)) 
  if (DriverLineId) then
    DriverLineId = DriverLineId + 1
  else
    DriverLineId = 1
  end

  N5183B = {}
  N5183B.Line     = DriverLineId
  N5183B.OnError  = nil
  N5183B.SetFreq  = N5183B_SetFreq
  N5183B.SetPower = N5183B_SetPower
  N5183B.SendSCPI = N5183B_SendCmd
  N5183B.Output   = N5183B_SetOutput
  N5183B.State = {LastCmd = "", LastValue = 0, Freq = 0, Power = -1000, Out = 0}

  if (IP) then -- nil --> use default
   N5183B_TCP_IP = IP
  end
  if (PORT) then -- nil --> use default
   N5183B_TCP_PORT = PORT
  end

  addObject("LINE",   "LINEGEN",  N5183B.Line, "[nUse]", N5183BLineProcTX)
  return N5183B
end


function N5183BLineProcTX(idEvent, buffer, LineName)
if (idEvent == "INIT") then
 mcLinkOpen (N5183B.Line, "TCP", N5183B_TCP_IP, tostring(N5183B_TCP_PORT), "DUMMY")
 end
return "OK", ""
end


